﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace NHISmobile
{
    public class ContactsAdapter : BaseAdapter
	{
	    public readonly List<Person> ContactList;
	    private readonly Activity _activity;

		public ContactsAdapter (Activity activity, List<Person> contacts)
		{
			_activity = activity;
			ContactList = contacts;
		}

		public override int Count => ContactList.Count;

	    public override Object GetItem (int position) {
			// could wrap a Contact in a Java.Lang.Object
			// to return it here if needed
			return null;
		}

		public override long GetItemId (int position) {
			return ContactList [position].Counter;
		}

		public Person GetPersonAtPosition(int position)
		{
			return ContactList [position];
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? _activity.LayoutInflater.Inflate (
				Resource.Layout.VCsavedRegItem, parent, false);
			var contactName = view.FindViewById<TextView> (Resource.Id.contactname);
			//var contactCounter = view.FindViewById<TextView> (Resource.Id.contactcounter);
			contactName.Text = ContactList [position].Firstname + " " + ContactList [position].Surname;
			//contactCounter.Text = _contactList [position].Counter.ToString() + ")";

			// Profile
			view.FindViewById<ImageView> (Resource.Id.hasprofile).SetImageResource (Resource.Drawable.ok);

			// login
		    view.FindViewById<ImageView>(Resource.Id.haslogin).SetImageResource(ContactList[position].LoginDone == 1
		        ? Resource.Drawable.ok
		        : Resource.Drawable.cancel);

		    // hospital
		    view.FindViewById<ImageView>(Resource.Id.hashospital).SetImageResource(ContactList[position].HospitalDone == 1
		        ? Resource.Drawable.ok
		        : Resource.Drawable.cancel);

		    // voucher
		    view.FindViewById<ImageView>(Resource.Id.hasvoucher).SetImageResource(ContactList[position].VoucherDone == 1
		        ? Resource.Drawable.ok
		        : Resource.Drawable.cancel);

		    // bio
		    view.FindViewById<ImageView>(Resource.Id.hasbio).SetImageResource(ContactList[position].BioDone == 1
		        ? Resource.Drawable.ok
		        : Resource.Drawable.cancel);

		    return view;
		}
	}
}

