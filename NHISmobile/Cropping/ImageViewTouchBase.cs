using System;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Environment = System.Environment;

namespace CropImage
{
    public abstract class ImageViewTouchBase : ImageView
    {
        #region Members

        // This is the base transformation which is used to show the image
        // initially.  The current computation for this shows the image in
        // it's entirety, letterboxing as needed.  One could choose to
        // show the image as cropped instead.
        //
        // This matrix is recomputed when we go from the thumbnail image to
        // the full size image.
        protected Matrix BaseMatrix = new Matrix();

        // This is the supplementary transformation which reflects what
        // the user has done in terms of zooming and panning.
        //
        // This matrix remains the same when we go from the thumbnail image
        // to the full size image.
        protected Matrix SuppMatrix = new Matrix();

        // This is the final matrix which is computed as the concatentation
        // of the base matrix and the supplementary matrix.
        private Matrix _displayMatrix = new Matrix();

        // Temporary buffer used for getting the values out of a matrix.
        private float[] _matrixValues = new float[9];

        // The current bitmap being displayed.
        protected RotateBitmap BitmapDisplayed = new RotateBitmap(null);

        private int _thisWidth = -1;
        private int _thisHeight = -1;
        const float ScaleRate = 1.25F;

        private float _maxZoom;

        private Handler _handler = new Handler();

        private Action _onLayoutRunnable;

        #endregion

        #region Constructor

        public ImageViewTouchBase(Context context)
            : base(context)
        {
            Init();
        }

        public ImageViewTouchBase(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Init();
        }

        #endregion

        #region Private helpers

        private void Init()
        {
            SetScaleType(ScaleType.Matrix);
        }

        private void setImageBitmap(Bitmap bitmap, int rotation)
        {
            base.SetImageBitmap(bitmap);
            var d = Drawable;
            if (d != null)
            {
                d.SetDither(true);
            }

            var old = BitmapDisplayed.Bitmap;
            BitmapDisplayed.Bitmap = bitmap;
            BitmapDisplayed.Rotation = rotation;
        }

        #endregion

        #region Public methods

        public void Clear()
        {
            SetImageBitmapResetBase(null, true);
        }

        /// <summary>
        /// This function changes bitmap, reset base matrix according to the size
        /// of the bitmap, and optionally reset the supplementary matrix.
        /// </summary>
        public void SetImageBitmapResetBase(Bitmap bitmap, bool resetSupp)
        {
            SetImageRotateBitmapResetBase(new RotateBitmap(bitmap), resetSupp);
        }

        public void SetImageRotateBitmapResetBase(RotateBitmap bitmap, bool resetSupp)
        {
            var viewWidth = Width;

            if (viewWidth <= 0)
            {
                _onLayoutRunnable = () =>
                {
                    SetImageRotateBitmapResetBase(bitmap, resetSupp);
                };

                return;
            }

            if (bitmap.Bitmap != null)
            {
                GetProperBaseMatrix(bitmap, BaseMatrix);
                setImageBitmap(bitmap.Bitmap, bitmap.Rotation);
            }
            else
            {
                BaseMatrix.Reset();
                base.SetImageBitmap(null);
            }

            if (resetSupp)
            {
                SuppMatrix.Reset();
            }
            ImageMatrix = GetImageViewMatrix();
            _maxZoom = CalculateMaxZoom();
        }


        #endregion

        #region Overrides

        protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
        {
            IvLeft = left;
            IvRight = right;
            IvTop = top;
            IvBottom = bottom;

            _thisWidth = right - left;
            _thisHeight = bottom - top;

            var r = _onLayoutRunnable;

            if (r != null)
            {
                _onLayoutRunnable = null;
                r();
            }

            if (BitmapDisplayed.Bitmap != null)
            {
                GetProperBaseMatrix(BitmapDisplayed, BaseMatrix);
                ImageMatrix = GetImageViewMatrix();
            }
        }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back && GetScale() > 1.0f)
            {
                // If we're zoomed in, pressing Back jumps out to show the entire
                // image, otherwise Back returns the user to the gallery.
                ZoomTo(1.0f);
                return true;
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void SetImageBitmap(Bitmap bm)
        {
            setImageBitmap(bm, 0);
        }

        #endregion

        #region Properties

        public int IvLeft { get; private set; }

        public int IvRight { get; private set; }

        public int IvTop { get; private set; }

        public int IvBottom { get; private set; }

        #endregion

        #region Protected methods

        protected float GetValue(Matrix matrix, int whichValue)
        {
            matrix.GetValues(_matrixValues);
            return _matrixValues[whichValue];
        }

        /// <summary>
        /// Get the scale factor out of the matrix.
        /// </summary>
        protected float GetScale(Matrix matrix)
        {
            return GetValue(matrix, Matrix.MscaleX);
        }

        protected float GetScale()
        {
            return GetScale(SuppMatrix);
        }

        /// <summary>
        /// Setup the base matrix so that the image is centered and scaled properly.
        /// </summary>
        private void GetProperBaseMatrix(RotateBitmap bitmap, Matrix matrix)
        {
            float viewWidth = Width;
            float viewHeight = Height;

            float w = bitmap.Width;
            float h = bitmap.Height;
            var rotation = bitmap.Rotation;
            matrix.Reset();

            // We limit up-scaling to 2x otherwise the result may look bad if it's
            // a small icon.
            var widthScale = Math.Min(viewWidth / w, 2.0f);
            var heightScale = Math.Min(viewHeight / h, 2.0f);
            var scale = Math.Min(widthScale, heightScale);

            matrix.PostConcat(bitmap.GetRotateMatrix());
            matrix.PostScale(scale, scale);

            matrix.PostTranslate(
                (viewWidth - w * scale) / 2F,
                (viewHeight - h * scale) / 2F);
        }

        // Combine the base matrix and the supp matrix to make the final matrix.
        protected Matrix GetImageViewMatrix()
        {
            // The final matrix is computed as the concatentation of the base matrix
            // and the supplementary matrix.
            _displayMatrix.Set(BaseMatrix);
            _displayMatrix.PostConcat(SuppMatrix);

            return _displayMatrix;
        }

        // Sets the maximum zoom, which is a scale relative to the base matrix. It
        // is calculated to show the image at 400% zoom regardless of screen or
        // image orientation. If in the future we decode the full 3 megapixel image,
        // rather than the current 1024x768, this should be changed down to 200%.
        protected float CalculateMaxZoom()
        {
            if (BitmapDisplayed.Bitmap == null)
            {
                return 1F;
            }

            var fw = BitmapDisplayed.Width / (float)_thisWidth;
            var fh = BitmapDisplayed.Height / (float)_thisHeight;
            var max = Math.Max(fw, fh) * 4;

            return max;
        }

        protected virtual void ZoomTo(float scale, float centerX, float centerY)
        {
            if (scale > _maxZoom)
            {
                scale = _maxZoom;
            }

            var oldScale = GetScale();
            var deltaScale = scale / oldScale;

            SuppMatrix.PostScale(deltaScale, deltaScale, centerX, centerY);
            ImageMatrix = GetImageViewMatrix();
            Center(true, true);
        }

        protected void ZoomTo(float scale, float centerX,
                               float centerY, float durationMs)
        {
            var incrementPerMs = (scale - GetScale()) / durationMs;
            var oldScale = GetScale();

            long startTime = Environment.TickCount;

            Action anim = null;

            anim = () =>
            {
                long now = Environment.TickCount;
                var currentMs = Math.Min(durationMs, now - startTime);
                var target = oldScale + (incrementPerMs * currentMs);
                ZoomTo(target, centerX, centerY);

                if (currentMs < durationMs)
                {
                    _handler.Post(anim);
                }
            };

            _handler.Post(anim);
        }

        protected void ZoomTo(float scale)
        {
            var cx = Width / 2F;
            var cy = Height / 2F;

            ZoomTo(scale, cx, cy);
        }

        protected virtual void ZoomIn()
        {
            ZoomIn(ScaleRate);
        }

        protected virtual void ZoomOut()
        {
            ZoomOut(ScaleRate);
        }

        protected virtual void ZoomIn(float rate)
        {
            if (GetScale() >= _maxZoom)
            {
                // Don't let the user zoom into the molecular level.
                return;
            }

            if (BitmapDisplayed.Bitmap == null)
            {
                return;
            }

            var cx = Width / 2F;
            var cy = Height / 2F;

            SuppMatrix.PostScale(rate, rate, cx, cy);
            ImageMatrix = GetImageViewMatrix();
        }

        protected void ZoomOut(float rate)
        {
            if (BitmapDisplayed.Bitmap == null)
            {
                return;
            }

            var cx = Width / 2F;
            var cy = Height / 2F;

            // Zoom out to at most 1x.
            var tmp = new Matrix(SuppMatrix);
            tmp.PostScale(1F / rate, 1F / rate, cx, cy);

            if (GetScale(tmp) < 1F)
            {
                SuppMatrix.SetScale(1F, 1F, cx, cy);
            }
            else
            {
                SuppMatrix.PostScale(1F / rate, 1F / rate, cx, cy);
            }

            ImageMatrix = GetImageViewMatrix();
            Center(true, true);
        }

        protected virtual void PostTranslate(float dx, float dy)
        {
            SuppMatrix.PostTranslate(dx, dy);
        }

        protected void PanBy(float dx, float dy)
        {
            PostTranslate(dx, dy);
            ImageMatrix = GetImageViewMatrix();
        }


        /// <summary>
        /// Center as much as possible in one or both axis.  Centering is
        /// defined as follows:  if the image is scaled down below the
        /// view's dimensions then center it (literally).  If the image
        /// is scaled larger than the view and is translated out of view
        /// then translate it back into view (i.e. eliminate black bars).
        /// </summary>
        protected void Center(bool horizontal, bool vertical)
        {
            if (BitmapDisplayed.Bitmap == null)
            {
                return;
            }

            var m = GetImageViewMatrix();

            var rect = new RectF(0, 0,
                                   BitmapDisplayed.Bitmap.Width,
                                   BitmapDisplayed.Bitmap.Height);

            m.MapRect(rect);

            var height = rect.Height();
            var width = rect.Width();

            float deltaX = 0, deltaY = 0;

            if (vertical)
            {
                var viewHeight = Height;
                if (height < viewHeight)
                {
                    deltaY = (viewHeight - height) / 2 - rect.Top;
                }
                else if (rect.Top > 0)
                {
                    deltaY = -rect.Top;
                }
                else if (rect.Bottom < viewHeight)
                {
                    deltaY = Height - rect.Bottom;
                }
            }

            if (horizontal)
            {
                var viewWidth = Width;
                if (width < viewWidth)
                {
                    deltaX = (viewWidth - width) / 2 - rect.Left;
                }
                else if (rect.Left > 0)
                {
                    deltaX = -rect.Left;
                }
                else if (rect.Right < viewWidth)
                {
                    deltaX = viewWidth - rect.Right;
                }
            }

            PostTranslate(deltaX, deltaY);
            ImageMatrix = GetImageViewMatrix();
        }


        #endregion
    }
}