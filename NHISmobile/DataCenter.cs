﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using VCmobileEntities;

namespace NHISmobile
{
    public static class DataCenter
	{
		#region Variables

		private static SqliteConnection _conn;
        private static bool _returnId;

		#endregion

		#region general

		private static void CreateSqlConnection ()
		{
			var documents = Environment.GetFolderPath (
				Environment.SpecialFolder.Personal);
			var db = Path.Combine (documents, "nhisdb4.db3");
			var exists = File.Exists (db);
			_conn = new SqliteConnection ("Data Source=" + db);

			// Create database if doesnt exist
			if (!exists) {
				// create sqlite file
				SqliteConnection.CreateFile (db);

				// create default tables
				var commands = new List<string> ();
                commands.Add("CREATE TABLE Person (ID INTEGER PRIMARY KEY, FirstName TEXT, Surname TEXT, MidName TEXT, Email TEXT, NatID TEXT, Phone TEXT, Dob TEXT, Gender TEXT, Marital TEXT, Photo TEXT, Fingerp TEXT, Sign TEXT, IsComplete INTEGER DEFAULT 0, IsUploaded INTEGER DEFAULT 0, Created Text, Username Text, Password Text, State Text, LGA Text, Hcp Text, Logindone INTEGER DEFAULT 0, Hcpdone INTEGER DEFAULT 0, VoucherNum INTEGER DEFAULT 0, VoucherPIN Text, Voucherdone INTEGER DEFAULT 0)");
				commands.Add("INSERT INTO Person (Firstname, Surname, MidName, Email, NatID, Phone, Dob, Gender, Marital, Created) VALUES ('Ayo', 'Fago', 'Tunde','ayo@irissmart.com', '55445', '08064455845', '12/05/1982', 'Male', 'Married', '09/02/2014 11:00 AM')");
				commands.Add("INSERT INTO Person (Firstname, Surname, MidName, Email, NatID, Phone, Dob, Gender, Marital, Created, Username, Password, Logindone, Hcpdone) VALUES ('Dami', 'Lawal', 'Bashir','dami@exarotech.com', '1234', '08094562884', '12/05/1980', 'Male', 'Single', '03/07/2014 11:10 AM', 'nastradl', 'Dami123', 1, 0)");
                commands.Add("INSERT INTO Person (Firstname, Surname, MidName, Email, NatID, Phone, Dob, Gender, Marital, Created, Username, Password, Logindone, State, Lga, Hcp, Hcpdone, VoucherNum, VoucherPIN, Voucherdone) VALUES ('Christy', 'Umoren', 'Dongesit','christy@irissmart.com', '33221', '08169180045', '12/05/1984', 'Female', 'Single', '04/07/2014 4:30 PM', 'christy', 'Chris123', 1, 'FCT', 'Municipal', 'Abuja Clinic - Maitama', 1, 402, 'AX34EDT3FF5FDDE9',1)");

				try
				{
					_conn.Open ();
					foreach (var cmd in commands) {
						using (var c = _conn.CreateCommand()) {
							c.CommandText = cmd;
							c.ExecuteNonQuery ();
						}
					}

					_conn.Close ();	
				}
				catch(Exception) {
					// ensure connection closes
					_conn.Close ();
				}
			}
		}

        private static int ExecuteQuery(string query, IDataParameter[] param)
        {
            var result = 0;

            try
            {
                _conn.Open();
                using (var c = _conn.CreateCommand())
                {
                    c.CommandText = query;
                    if (param != null)
                        c.Parameters.AddRange(param);

                    c.ExecuteNonQuery();

                    if (_returnId)
                    {
                        _returnId = false;

                        // Get last insert Id
                        c.CommandText = "SELECT last_insert_rowid()";
                        result = Convert.ToInt32(c.ExecuteScalar());
                    }
                }

                _conn.Close();
            }
            catch (Exception)
            {
                // ensure connection closes
                _conn.Close();
                return 0;
            }

            return result;
        }

		private static int QueryData(string query, IDataParameter[] param)
		{
			int result;

			try
			{
				_conn.Open ();
				using (var c = _conn.CreateCommand()) {
					c.CommandText = query;
					if (param != null)
						c.Parameters.AddRange(param);
					result = c.ExecuteNonQuery ();
				}

				_conn.Close ();
			}
			catch(Exception) {
				// ensure connection closes
				_conn.Close ();
				return 0;
			}

			return result;
		}

		#endregion

		#region VC

		public static List<Person> GetSavedVcRegistrations()
		{
			var result = new List<Person> ();
            var query = "select ID, Firstname, Surname, MidName, Email, NatID, Phone, Dob, Gender, Marital, Created, Username, Password, Logindone, State, Lga, Hcp, VoucherNum, VoucherPIN, Voucherdone, Hcpdone, Voucherdone, Photo, Sign, Fingerp from Person where IsUploaded = 0";

			try
			{
				if (_conn == null)
					CreateSqlConnection();

			    if (_conn != null)
			    {
			        _conn.Open ();
			        using (var c = _conn.CreateCommand()) {
			            c.CommandText = query;
			            var i = 1;
			            var reader = c.ExecuteReader();
			            while(reader.Read()) {
			                var p = new Person
			                {
			                    Counter = i,
			                    ProfileDone = 1,
			                    PersonId = reader.GetInt32(reader.GetOrdinal("ID")),
			                    Firstname = reader.SafeGetString(reader.GetOrdinal("Firstname")),
			                    Surname = reader.SafeGetString(reader.GetOrdinal("Surname")),
			                    Middlename = reader.SafeGetString(reader.GetOrdinal("MidName")),
			                    Email = reader.SafeGetString(reader.GetOrdinal("Email")),
			                    NatId = reader.SafeGetString(reader.GetOrdinal("NatID")),
			                    Phonenum = reader.SafeGetString(reader.GetOrdinal("Phone")),
			                    Dob = reader.SafeGetString(reader.GetOrdinal("Dob")),
			                    Gender = reader.SafeGetString(reader.GetOrdinal("Gender")),
			                    Marital = reader.SafeGetString(reader.GetOrdinal("Marital")),
			                    Created = reader.SafeGetString(reader.GetOrdinal("Created")),
			                    Photo = reader.SafeGetString(reader.GetOrdinal("Photo")),
			                    Signature = reader.SafeGetString(reader.GetOrdinal("Sign")),
			                    Fingerprint = reader.SafeGetString(reader.GetOrdinal("Fingerp")),
			                    LoginDone = reader.GetInt32(reader.GetOrdinal("Logindone"))
			                };

			                if (p.LoginDone == 1)
			                {
			                    p.Username = reader.SafeGetString(reader.GetOrdinal("Username"));
			                    p.Password = reader.SafeGetString(reader.GetOrdinal("Password"));
			                }

			                p.HospitalDone = reader.GetInt32(reader.GetOrdinal("Hcpdone"));
			                if (p.HospitalDone == 1)
			                {
			                    p.State = new State
			                    { StateName = reader.SafeGetString(reader.GetOrdinal("State")), 
			                        SelectedLga = new Lga
			                        { LgAname = reader.SafeGetString(reader.GetOrdinal("Lga")),
			                            SelectedHospital = reader.SafeGetString(reader.GetOrdinal("Hcp"))
			                        }
			                    };
			                }

			                p.VoucherDone = reader.GetInt32(reader.GetOrdinal("Voucherdone"));
			                if (p.VoucherDone == 1)
			                {
			                    p.VoucherNumber = reader.GetInt32(reader.GetOrdinal("VoucherNum"));
			                    p.VoucherPin = reader.SafeGetString(reader.GetOrdinal("VoucherPIN"));
			                }

			                if (!string.IsNullOrEmpty(p.Photo) && !string.IsNullOrEmpty(p.Fingerprint) && !string.IsNullOrEmpty(p.Signature))
			                {
			                    p.BioDone = 1;
			                }

			                result.Add(p);
			                i++;
			            }
			        }

			        _conn.Close ();
			    }
			}
			catch(Exception) {
				// ensure connection closes
			    _conn?.Close();
				return null;
			}

			return result;
		}

        public static List<Person> GetCompleteSavedVcRegistrations()
        {
            var result = new List<Person>();
            var query = "select ID, Firstname, Surname, MidName, Email, NatID, Phone, Dob, Gender, Marital, Created, Username, Password, Logindone, State, Lga, Hcp, VoucherNum, VoucherPIN, Voucherdone, Hcpdone, Voucherdone, Photo, Sign, Fingerp from Person where iscomplete = 1";

            try
            {
                if (_conn == null)
                    CreateSqlConnection();

                if (_conn != null)
                {
                    _conn.Open();
                    using (var c = _conn.CreateCommand())
                    {
                        c.CommandText = query;
                        var i = 1;
                        var reader = c.ExecuteReader();
                        while (reader.Read())
                        {
                            var p = new Person
                            {
                                Counter = i,
                                ProfileDone = 1,
                                PersonId = reader.GetInt32(reader.GetOrdinal("ID")),
                                Firstname = reader.SafeGetString(reader.GetOrdinal("Firstname")),
                                Surname = reader.SafeGetString(reader.GetOrdinal("Surname")),
                                Middlename = reader.SafeGetString(reader.GetOrdinal("MidName")),
                                Email = reader.SafeGetString(reader.GetOrdinal("Email")),
                                NatId = reader.SafeGetString(reader.GetOrdinal("NatID")),
                                Phonenum = reader.SafeGetString(reader.GetOrdinal("Phone")),
                                Dob = reader.SafeGetString(reader.GetOrdinal("Dob")),
                                Gender = reader.SafeGetString(reader.GetOrdinal("Gender")),
                                Marital = reader.SafeGetString(reader.GetOrdinal("Marital")),
                                Created = reader.SafeGetString(reader.GetOrdinal("Created")),
                                Photo = reader.SafeGetString(reader.GetOrdinal("Photo")),
                                Signature = reader.SafeGetString(reader.GetOrdinal("Sign")),
                                Fingerprint = reader.SafeGetString(reader.GetOrdinal("Fingerp")),
                                LoginDone = reader.GetInt32(reader.GetOrdinal("Logindone"))
                            };

                            if (p.LoginDone == 1)
                            {
                                p.Username = reader.SafeGetString(reader.GetOrdinal("Username"));
                                p.Password = reader.SafeGetString(reader.GetOrdinal("Password"));
                            }

                            p.HospitalDone = reader.GetInt32(reader.GetOrdinal("Hcpdone"));
                            if (p.HospitalDone == 1)
                            {
                                p.State = new State
                                {
                                    StateName = reader.SafeGetString(reader.GetOrdinal("State")),
                                    SelectedLga = new Lga
                                    {
                                        LgAname = reader.SafeGetString(reader.GetOrdinal("Lga")),
                                        SelectedHospital = reader.SafeGetString(reader.GetOrdinal("Hcp"))
                                    }
                                };
                            }

                            p.VoucherDone = reader.GetInt32(reader.GetOrdinal("Voucherdone"));
                            if (p.VoucherDone == 1)
                            {
                                p.VoucherNumber = reader.GetInt32(reader.GetOrdinal("VoucherNum"));
                                p.VoucherPin = reader.SafeGetString(reader.GetOrdinal("VoucherPIN"));
                            }

                            if (!string.IsNullOrEmpty(p.Photo) && !string.IsNullOrEmpty(p.Fingerprint) && !string.IsNullOrEmpty(p.Signature))
                            {
                                p.BioDone = 1;
                            }

                            result.Add(p);
                            i++;
                        }
                    }

                    _conn.Close();
                }
            }
            catch (Exception)
            {
                // ensure connection closes
                _conn?.Close();
                return null;
            }

            return result;
        }

        public static long GetLastInsertId()
        {
            long result = 0;

            if (_conn == null)
                CreateSqlConnection();

            var query = "SELECT last_insert_rowid()";
            if (_conn != null)
            {
                _conn.Open();
                using (var c = _conn.CreateCommand())
                {
                    c.CommandText = query;
                    result = (long)c.ExecuteScalar();
                }

                _conn.Close();
            }

            return result;
        }

		public static int DeleteSavedProfile (Person person)
		{
			if (_conn == null)
				CreateSqlConnection ();

			// Create table if doesnt exist
			var query = "DELETE FROM Person WHERE ID = @ID";
			IDataParameter[] param =
			{   
				new SqliteParameter("ID",  person.PersonId)
			};

			ExecuteQuery (query, param);

			return 1;
		}

		public static int SaveVcRegisterProfile (Person person)
		{
			if (_conn == null)
				CreateSqlConnection ();

			// Create table if doesnt exist
			var query = "INSERT INTO Person (Firstname, Surname, MidName, Email, NatID, Phone, Dob, Gender) VALUES (@Firstname, @Surname, @MidName, @Email, @NatID, @Phone, @Dob, @Gender)";
			IDataParameter[] param =
			{   
				new SqliteParameter("Firstname",  person.Firstname),
				new SqliteParameter("Surname",  person.Surname),
				new SqliteParameter("MidName",  person.Middlename),
				new SqliteParameter("Email",  person.Email),
				new SqliteParameter("NatID",  person.NatId),
				new SqliteParameter("Phone",  person.Phonenum),
				new SqliteParameter("Dob",  person.Dob),
				new SqliteParameter("Gender",  person.Gender)
			};

            _returnId = true;
            return ExecuteQuery(query, param);
		}

		public static void UpdateVcRegisterProfile (Person person)
		{
			if (_conn == null)
				CreateSqlConnection ();

			// Create table if doesnt exist
            var query = "update Person set Firstname = @Firstname, Surname = @Surname, MidName = @MidName, Email = @Email, NatID = @NatID, Phone = @Phone, Dob = @Dob, Gender = @Gender, Photo = @Photo, Fingerp = @Fingerp, Sign = @Sign, iscomplete = 1 where ID = @ID";
			IDataParameter[] param =
			{   
				new SqliteParameter("Firstname",  person.Firstname),
				new SqliteParameter("Surname",  person.Surname),
				new SqliteParameter("MidName",  person.Middlename),
				new SqliteParameter("Email",  person.Email),
				new SqliteParameter("NatID",  person.NatId),
				new SqliteParameter("Phone",  person.Phonenum),
				new SqliteParameter("Dob",  person.Dob),
				new SqliteParameter("Gender",  person.Gender),
				new SqliteParameter("ID",  person.PersonId),
                new SqliteParameter("Fingerp",  person.Fingerprint),
                new SqliteParameter("Photo",  person.Photo),
                new SqliteParameter("Sign",  person.Signature)
			};

			ExecuteQuery (query, param);
		}

        public static void SetRegistrationStatusToUploaded(long personid)
        {
            if (_conn == null)
                CreateSqlConnection();

            // Create table if doesnt exist
            var query = "update Person set IsUploaded = 1 where ID = @ID";
            IDataParameter[] param =
			{   
				new SqliteParameter("ID",  personid)
			};

            ExecuteQuery(query, param);
        }

		public static void SaveVcUsername (Person person)
		{
			if (_conn == null)
				CreateSqlConnection ();

			// Create table if doesnt exist
            var query = "update Person set username = @username, password = @password, Logindone = 1 where id = @id";
			IDataParameter[] param =
			{   
				new SqliteParameter("username",  person.Username),
				new SqliteParameter("password",  person.Password),
				new SqliteParameter("id",  person.PersonId)
			};

			ExecuteQuery (query, param);
		}

		public static void SaveVcHospital (Person person)
		{
			if (_conn == null)
				CreateSqlConnection ();

			// Update details
            var query = "update Person set State = @State, LGA = @LGA, hcp = @hcp, hcpdone = 1 where ID = @ID";
			IDataParameter[] param =
			{   
				new SqliteParameter("State",  person.State.StateName),
				new SqliteParameter("LGA",  person.State.SelectedLga.LgAname),
				new SqliteParameter("hcp",  person.State.SelectedLga.SelectedHospital),
				new SqliteParameter("ID",  person.PersonId)
			};

			ExecuteQuery (query, param);
		}

		public static void SaveVcVoucher (Person person)
		{
			if (_conn == null)
				CreateSqlConnection ();

			// Update details
            var query = "update Person set VoucherNum = @VoucherNum, VoucherPIN = @VoucherPIN, voucherdone = 1 where id = @id";
			IDataParameter[] param =
			{   
				new SqliteParameter("VoucherNum",  person.VoucherNumber),
				new SqliteParameter("VoucherPIN",  person.VoucherPin),
				new SqliteParameter("id",  person.PersonId)
			};

			ExecuteQuery (query, param);
		}

		#endregion			
      
    }
}

