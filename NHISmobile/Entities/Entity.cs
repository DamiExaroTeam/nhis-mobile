using System.Runtime.Serialization;

namespace VCmobileEntities
{
    [DataContract]
    public class Entity
    {
        [DataMember]
        public long EntityVoluntaryId { get; set; }

        [DataMember]
        public long HcfId { get; set; }

        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Middlename { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string MarStatus { get; set; }

        [DataMember]
        public string DobText { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string NationalId { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string GsmNumber { get; set; }

        [DataMember]
        public string Hcf { get; set; }

        [DataMember]
        public string Hmo { get; set; }

        [DataMember]
        public string StateText { get; set; }

        [DataMember]
        public string LgaText { get; set; }
    }
}