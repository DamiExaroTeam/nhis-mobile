using System;
using System.Runtime.Serialization;

namespace VCmobileEntities
{
    [DataContract]
    public class Hcf
    {
        [DataMember]
        public long HcfId { get; set; }

        [DataMember]
        public string HcfName { get; set; }

        public long GetHcfId() { return HcfId; }

        public String GetHcfName() { return HcfName; }
    }
}