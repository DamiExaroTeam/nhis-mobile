﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VCmobileEntities
{
    [DataContract]
	public class Lga
	{
        // Serialisation
        [DataMember]
        public long LgaId { get; set; }

        [DataMember]
        public string StateAbr { get; set; }

        [DataMember]
        public string LgaName { get; set; }


        // My stuff
        public long GetLgaId() { return LgaId; }

        public String GetStateAbr() { return StateAbr; }

        public String GetLgaName() { return LgaName; }

		public string LgAname { get; set; }
		public List<string> Hospitals {get; set; }
		public string SelectedHospital { get; set; }

		public override string ToString ()
		{
			return LgAname;
		}
	}
}

