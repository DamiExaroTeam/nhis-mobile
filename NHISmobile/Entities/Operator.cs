﻿namespace NHISmobile
{
	public class Operator
	{
		public string Firstname { get; set; }
		public string Surname { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
	}
}

