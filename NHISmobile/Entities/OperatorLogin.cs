using System.Runtime.Serialization;

namespace VCmobileEntities
{
    [DataContract]
    public class OperatorLogin
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string MacAddress { get; set; }
    }
}