﻿using VCmobileEntities;

namespace NHISmobile
{
	public class Person
	{
		// Personal Details
		public int Counter { get; set; }
		public long PersonId { get; set; }
		public string Firstname { get; set; }
		public string Surname { get; set; }
		public string Middlename { get; set; }
		public string Phonenum { get; set; }
		public string Email { get; set; }

		public string NhiSnum { get; set; }

		public string Dob { get; set; }
		public string NatId { get; set; }
		public string Gender { get; set; }
		public string Marital { get; set; }
		public string Created { get; set; }
		public int ProfileDone { get; set; }

		public string Signature { get; set; }

		public string Fingerprint { get; set; }

		public byte[] PhotoByte { get; set; }

		public string PhotoByteZipped { get; set; }

		public byte[] SignatureByte { get; set; }

		public string SignatureByteZipped { get; set; }

		public byte[] FingerprintByte { get; set; }

		public string FingerprintByteZipped { get; set; }

		public string Photo { get; set; }

		// login
		public string Username { get; set; }
		public string Password { get; set; }
		public int LoginDone { get; set; }

		public string Security { get; set; }

		// HMO
		public string Hmo { get; set; }

		public string CoverFrom { get; set; }

		public string CoverTo { get; set; }

		// Hospital
		public State State { get; set; }
		public Lga Lga { get; set; }
		public string Hospital { get; set; }
		public int HospitalDone { get; set; }

		// Voucher
		public int VoucherNumber { get; set; }
		public string VoucherPin { get; set; }
		public int VoucherDone { get; set; }

		// Biometrics
		public int BioDone { get; set; }

		public override string ToString()
		{
			return string.Format("{0} {1} (Created {2})", Firstname, Surname, Created);
		}
	}
}

