using System;
using System.Runtime.Serialization;

namespace VCmobileEntities
{
    [DataContract]
    public class RegistrationResult
    {
        [DataMember]
        public long EntityId { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public bool SmsSent { get; set; }

        public long GetEntityId() { return EntityId; }

        public String GetMessage() { return Message; }

        public bool GetSmsSent() { return SmsSent; }
    }
}