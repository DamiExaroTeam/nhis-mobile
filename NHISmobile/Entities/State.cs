﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VCmobileEntities
{
    [DataContract]
	public class State
	{
        [DataMember]
        public string StateAbbreviation { get; set; }

        [DataMember]
        public string StateName { get; set; }


		public List<Lga> LgAs {get; set; }

		public Lga SelectedLga { get; set; }

		public override string ToString ()
		{
			return StateName;
		}
	}
}

