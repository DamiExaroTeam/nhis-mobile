﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using Android.Animation;
using Android.Graphics;
using Android.Media;
using Android.Widget;
using Mono.Data.Sqlite;
using Encoding = System.Text.Encoding;

namespace NHISmobile
{
    public static class Helper
    {
        public static Operator Operator { get; set; }

        public static int TimeDifference { get; set; }

        public static string GetDateTimeNow()
        {
            return DateTime.Now.AddMinutes(TimeDifference + 1).ToString("ddMMyyyyhhmm");
        }

        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T DeserializeJson<T>(string json)
        {
            var obj = Activator.CreateInstance<T>();
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serializer = new DataContractJsonSerializer(obj.GetType());
                obj = (T)serializer.ReadObject(ms);
                ms.Close();
            }

            return obj;
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }

        public static string SafeGetString(this SqliteDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        public static ValueAnimator SlideAnimator(int start, int end, LinearLayout mLinearLayout)
        {
            var animator = ValueAnimator.OfInt(start, end);
            animator.Update +=
                (sender, e) =>
                {
                    var value = (int)animator.AnimatedValue;
                    var layoutParams = mLinearLayout.LayoutParameters;
                    layoutParams.Height = value;
                    mLinearLayout.LayoutParameters = layoutParams;

                };

            return animator;
        }

        public static void SetFace(Bitmap mFaceBitmap, ImageView view)
        {
            var mFaceWidth = mFaceBitmap.Width;
            var mFaceHeight = mFaceBitmap.Height;
            FaceDetector fd;
            var faces = new FaceDetector.Face[1];
            var midpoint = new PointF();
            int[] fpx = null;
            int[] fpy = null;
            int count;


            try
            {
                fd = new FaceDetector(mFaceWidth, mFaceHeight, 1);
                count = fd.FindFaces(mFaceBitmap, faces);
            }
            catch (Exception)
            {
                return;
            }

            // check if we detect any faces
            if (count > 0)
            {
                fpx = new int[count];
                fpy = new int[count];


                for (var i = 0; i < count; i++)
                {
                    faces[i].GetMidPoint(midpoint);
                    fpx[i] = (int)midpoint.X;
                    fpy[i] = (int)midpoint.Y;
                }
            }

            // Set display points
            if (fpx != null && count > 0)
            {
                var mPx = new int[count];
                var mPy = new int[count];

                for (var i = 0; i < count; i++)
                {
                    mPx[i] = fpx[i];
                    mPy[i] = fpy[i];
                }
            }
        }
    }

	public static class Hashing
	{
		public static byte[] Hash(string value, byte[] salt)
		{
			return Hash(Encoding.UTF8.GetBytes(value), salt);
		}

		public static byte[] Hash(byte[] value, byte[] salt)
		{
			var saltedValue = value.Concat(salt).ToArray();
			return new SHA256Managed().ComputeHash(saltedValue);
		}

		public static bool ValidateHash(byte[] hashedval)
		{
			var passwordHash = Hash("NHISmobileApp", Encoding.UTF8.GetBytes(DateTime.Now.ToString("ddMMyyyyhhmm")));

			return hashedval.SequenceEqual(passwordHash);
		}
	}

	public static class StringCipher
	{
		// This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
		// This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
		// 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
		private static readonly byte[] InitVectorBytes = Encoding.ASCII.GetBytes("ef34ewnj433y33x1");

		// This constant is used to determine the keysize of the encryption algorithm.
		private const int Keysize = 256;

		public static string Encrypt(string plainText, string passPhrase)
		{
			var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			using (var password = new PasswordDeriveBytes(passPhrase, null))
			{
				var keyBytes = password.GetBytes(Keysize / 8);
				using (var symmetricKey = new RijndaelManaged())
				{
					symmetricKey.Mode = CipherMode.CBC;
					using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, InitVectorBytes))
					{
						using (var memoryStream = new MemoryStream())
						{
							using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
							{
								cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
								cryptoStream.FlushFinalBlock();
								var cipherTextBytes = memoryStream.ToArray();
								return Convert.ToBase64String(cipherTextBytes);
							}
						}
					}
				}
			}
		}

		public static string Decrypt(string cipherText, string passPhrase)
		{
			var cipherTextBytes = Convert.FromBase64String(cipherText);
			using(var password = new PasswordDeriveBytes(passPhrase, null))
			{
				var keyBytes = password.GetBytes(Keysize / 8);
				using(var symmetricKey = new RijndaelManaged())
				{
					symmetricKey.Mode = CipherMode.CBC;
					using(var decryptor = symmetricKey.CreateDecryptor(keyBytes, InitVectorBytes))
					{
						using(var memoryStream = new MemoryStream(cipherTextBytes))
						{
							using(var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
							{
								var plainTextBytes = new byte[cipherTextBytes.Length];
								var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
								return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
							}
						}
					}
				}
			}
		}
	}
}

