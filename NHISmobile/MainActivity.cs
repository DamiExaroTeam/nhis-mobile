﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NHISmobile
{
    [Activity(Label = "NHISmobile", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            SetContentView(Resource.Layout.Main);

            // Setup button events
            SetButtons();

            // Get server time
            Helper.TimeDifference = -1;
            await Task.Run(() => MobileService.GetServerTime()).ContinueWith(prevTask =>
            {
                if (!string.IsNullOrEmpty(prevTask.Result))
					{
						DateTime outval;
						if (DateTime.TryParse(prevTask.Result, out outval))
						{
							Helper.TimeDifference = outval.Subtract(DateTime.Now).Minutes;

//							RunOnUiThread (() => 
//								{
//									Toast toast = Toast.MakeText(Application.Context, prevTask.Result + " - Time difference", ToastLength.Long);
//									toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
//									toast.Show();
//								});
						}
					}
            });
        }

        #region Operations

        public void SetButtons()
        {
            // On VC Click
            var tv = FindViewById<TextView>(Resource.Id.vctextview);
            tv.Click += delegate
            {
                CollapseVc();
            };

            // On VC Registration Click
            var btn = FindViewById<Button>(Resource.Id.regBtn);
            btn.Click += delegate
            {
                StartActivity(typeof(VCregistration));
            };

            // On VC View saved registrations Click
            btn = FindViewById<Button>(Resource.Id.savedRegbtn);
            btn.Click += delegate
            {
                StartActivity(typeof(VCsavedReg));
            };

            // On FS Click
            var fs = FindViewById<TextView>(Resource.Id.fstextview);
            fs.Click += delegate
            {
                CollapseFs();
            };

            // On OP Click
            var op = FindViewById<TextView>(Resource.Id.optextview);
            op.Click += delegate
            {
                CollapseOp();
            };

            // On OP Login
            btn = FindViewById<Button>(Resource.Id.oploginBtn);
            btn.Click += delegate
            {
                //StartActivity(typeof(OPmainActivity));
                VerifyOpLogin();
            };

            // Sync clicked
            btn = FindViewById<Button>(Resource.Id.updatebtn);
            btn.Click += delegate
            {
                string toastmsg;

                // Get complete registrations from database
                var regs = DataCenter.GetCompleteSavedVcRegistrations();

                if (regs == null || regs.Count == 0)
                {
                    toastmsg = "No completed registrations to upload";

                    var toast = Toast.MakeText(Application.Context, toastmsg, ToastLength.Short);
                    toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                    toast.Show();
                }
                else
                {
                    var progressDialog = ProgressDialog.Show(this, "Please wait...", "Uploading data", true);
                    new Thread(new ThreadStart(delegate
                    {
                        // Send emails to people
                        foreach (var member in regs)
                        {
                            SendEmailToMember(member);

                            // Set registration status to uploaded
                            DataCenter.SetRegistrationStatusToUploaded(member.PersonId);
                        }

                        toastmsg = "Syncronisation complete";

                        RunOnUiThread(() =>
                            {
                                progressDialog.Hide();
                                var toast = Toast.MakeText(Application.Context, toastmsg, ToastLength.Short);
                                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                                toast.Show();
                            });
                    })).Start();
                }
            };

            // On sync
            //btn = FindViewById<Button>(Resource.Id.updatebtn);
            //btn.Click += delegate
            //{
            //    var progressDialog = ProgressDialog.Show(this, "Please wait...", "Uploading Saved Data", true);
            //    new Thread(new ThreadStart(delegate
            //    {
            //        RunOnUiThread(() => progressDialog.Hide());
            //    })).Start();
            //    //string test = MobileService.GetHmoForVoucher("401");
            //    //Entity result = MobileService.GetVoluntaryEntity("40000002");
            //    //long result = proxy.Channel.LoginMobileUser("nastradl", "damilola123", "12345");
            //};


        }

        public void SendEmailToMember(Person person)
        {
            var mail = new MailMessage();
            var friendlyName = "NHIS Mobile";

            var sb = new StringBuilder();
            sb.Append("<div style='margin-bottom: 15px'>Hello " + person.Firstname + ",</div>");
            sb.Append("<div style='margin-bottom: 15px'>Your registration has been completed on the NHIS Voluntary Contributor Scheme. Your NHIS number is <b>40000345</b> and your registered HMO is <b>Clearline Internation Healthcare.</b>"
            + ". Please don't hesistate to contact them if you have any issues collecting health services from your selected hospital - <b>" + person.State.SelectedLga.SelectedHospital + "</b>.</div>");
            sb.Append("<div>");
            sb.Append("<div>Regards,</div>");
            sb.Append("</div><div style='margin-top:25px'>");
            sb.Append("National Health Insurance Scheme (NHIS)");
            sb.Append("</div>");

            mail.From = new MailAddress("dami.b.lawal@gmail.com", friendlyName);
            //mail.To.Add(staffmember.EmailAddress);
            mail.To.Add("dami.b.lawal@gmail.com");
            mail.IsBodyHtml = true;

            //set the content
            mail.Subject = "NHIS Registration Complete";
            mail.Body = sb.ToString();

            var smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                Credentials = new NetworkCredential("dlawal@irissmart.com", "damilola123")
            };
            smtp.SendCompleted += SendCompletedCallback;
            smtp.SendAsync(mail, mail);
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            if (e.Cancelled)
            {

            }
            if (e.Error != null)
            {

            }
        }

        private async void VerifyOpLogin()
        {
            var username = FindViewById<EditText>(Resource.Id.opusername).Text;
            var password = FindViewById<EditText>(Resource.Id.oppassword).Text;

            var data = StringCipher.Encrypt(username + "|" + password, Helper.GetDateTimeNow());

            int result;
            using (var progressDialog = ProgressDialog.Show(this, "Please wait...", "Logging in", true))
            {
                progressDialog.Show();
                result = await Task.Run(() =>
                {
                    var res = MobileService.LoginOperator(data);

                    RunOnUiThread(() =>
                    {
                        progressDialog.Dismiss();
                    });

                    return res;
                });
            }

            if (result == 0 || result == -1)
            {
                var toast = Toast.MakeText(Application.Context, "Error logging into server. Please check connection and try again.", ToastLength.Short);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                toast.Show();
            }

            if (result == 1)
            {
                Helper.Operator.Username = username;
                Helper.Operator.Password = password;
                StartActivity(typeof(OPmainActivity));
            }

            if (result == 2)
            {
                var toast = Toast.MakeText(Application.Context, "Unauthorized Access", ToastLength.Long);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                toast.Show();
            }

            if (result == 3)
            {
                var toast = Toast.MakeText(Application.Context, "Invalid username or password", ToastLength.Long);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                toast.Show();
            }
        }

        public void CollapseVc()
        {
            var mLinearLayout = FindViewById<LinearLayout>(Resource.Id.vclogindiv);
            var tv = FindViewById<TextView>(Resource.Id.vctextview);

            CollapseLayout(mLinearLayout, tv);
        }

        public void CollapseFs()
        {
            var mLinearLayout = FindViewById<LinearLayout>(Resource.Id.fslogindiv);
            var tv = FindViewById<TextView>(Resource.Id.fstextview);

            CollapseLayout(mLinearLayout, tv);
        }

        public void CollapseOp()
        {
            var mLinearLayout = FindViewById<LinearLayout>(Resource.Id.oplogindiv);
            var tv = FindViewById<TextView>(Resource.Id.optextview);

            CollapseLayout(mLinearLayout, tv);
        }

        public void CollapseAll(LinearLayout layout)
        {
            for (var i = 0; i < 3; i++)
            {
                var lid = i == 0 ? Resource.Id.vclogindiv : i == 1 ? Resource.Id.fslogindiv : Resource.Id.oplogindiv;
                var tid = i == 0 ? Resource.Id.vctextview : i == 1 ? Resource.Id.fstextview : Resource.Id.optextview;

                var mLinearLayout = FindViewById<LinearLayout>(lid);
                var tv = FindViewById<TextView>(tid);

                if (mLinearLayout.Visibility.Equals(ViewStates.Visible) && mLinearLayout != layout)
                    ResetLayout(mLinearLayout, tv);
            }

            // Hide registration button
            FindViewById<Button>(Resource.Id.regBtn).Visibility = ViewStates.Gone;
            FindViewById<Button>(Resource.Id.updatebtn).Visibility = ViewStates.Gone;
            FindViewById<Button>(Resource.Id.savedRegbtn).Visibility = ViewStates.Gone;
        }

        public void ResetLayout(LinearLayout mLinearLayout, TextView tv)
        {
            var finalHeight = mLinearLayout.Height;

            var mAnimator = Helper.SlideAnimator(finalHeight, 0, mLinearLayout);
            mAnimator.Start();
            mAnimator.AnimationEnd += (intentSender, arg) =>
            {
                mLinearLayout.Visibility = ViewStates.Gone;
                tv.SetBackgroundResource(Resource.Drawable.allcorners);
                var paddingindp = (int)(30 / Resources.DisplayMetrics.Density);
                tv.SetPadding(paddingindp, paddingindp, paddingindp, paddingindp);
            };
        }

        public void CollapseLayout(LinearLayout mLinearLayout, TextView tv)
        {
            CollapseAll(mLinearLayout);

            if (mLinearLayout.Visibility.Equals(ViewStates.Gone))
            {
                // set Visible
                tv.SetBackgroundResource(Resource.Drawable.topcorners);
                var paddingindp = (int)(30 / Resources.DisplayMetrics.Density);
                tv.SetPadding(paddingindp, paddingindp, paddingindp, paddingindp);

                mLinearLayout.Visibility = ViewStates.Visible;
                var widthSpec = View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                var heightSpec = View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                mLinearLayout.Measure(widthSpec, heightSpec);

                var mAnimator = Helper.SlideAnimator(0, mLinearLayout.MeasuredHeight, mLinearLayout);
                mAnimator.Start();

                // show registration/continue/sync button
                if (mLinearLayout.Id == Resource.Id.vclogindiv)
                {
                    FindViewById<Button>(Resource.Id.regBtn).Visibility = ViewStates.Visible;
                    FindViewById<Button>(Resource.Id.updatebtn).Visibility = ViewStates.Visible;
                    FindViewById<Button>(Resource.Id.savedRegbtn).Visibility = ViewStates.Visible;
                }

            }
            else
            {
                ResetLayout(mLinearLayout, tv);
            }
        }

        #endregion
    }
}


