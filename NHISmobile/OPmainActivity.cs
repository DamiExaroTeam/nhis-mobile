﻿using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using System.Threading.Tasks;

namespace NHISmobile
{
    [Activity(Label = "NHIS mobile - Operators", Icon = "@drawable/icon2")]
    public class OPmainActivity : Activity
    {
        #region Variables

        private DrawerLayout _drawer;
        private MyActionBarDrawerToggle _drawerToggle;
        private ListView _drawerList;
        private Person _selectedItem;
        private Voucher _selectedVoucher;
        private bool _searching;

        private string _drawerTitle;
        private string _title;
        private string[] _menuTitles;

        #endregion

        #region Constructor

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.OPhome);

            // Define the drawer
            SetDrawer(bundle);

            SetButtons();
        }

        #endregion

        #region Operations

        private void SetButtons()
        {
            // On Person search 
            FindViewById<LinearLayout>(Resource.Id.searchmsg).Visibility = ViewStates.Gone;

            var btn = FindViewById<Button>(Resource.Id.opsearchbtn);
            var tv = FindViewById<TextView>(Resource.Id.searchheader);
            var mLinearLayout = FindViewById<LinearLayout>(Resource.Id.searchbox);

            var lv = (ListView)FindViewById(Resource.Id.opsearchresults);

            // Create event listener for click
            lv.ItemClick += listView_ItemClick;

            btn.Click += delegate
            {
                HideKeyboard();
                SearchForMembers();
            };

            // Search again
            tv.Click += delegate
            {
                if (tv.Text == "Search for another member")
                {
                    tv.Text = "Search for a member";
                    CollapseLayout(mLinearLayout, tv);
                    FindViewById<LinearLayout>(Resource.Id.searchresults).Visibility = ViewStates.Gone;
                }
            };

            // On view member
            FindViewById<Button>(Resource.Id.opviewbtn).Click += delegate
            {
                ShowMemberDetails();
            };

            // On view member (voucher)
            FindViewById<Button>(Resource.Id.opvoucherviewbtn).Click += delegate
            {
                ShowVoucherMemberDetails();
            };

            // On Voucher Search
            var vbtn = FindViewById<Button>(Resource.Id.opvsearchbtn);
            vbtn.Click += delegate
            {
                HideKeyboard();
                if (!_searching)
                    SearchForVoucher();
            };
        }

        private void HideKeyboard()
        {
            var imm = (InputMethodManager)GetSystemService(InputMethodService);
            imm.HideSoftInputFromWindow(CurrentFocus.WindowToken, 0);
        }

        private async void ShowMemberDetails()
        {
            var bar = FindViewById<ProgressBar>(Resource.Id.viewprogress);
            bar.Visibility = ViewStates.Visible;
            _searching = true;

            // Get member biometrics
            await Task.Run(() => MobileService.GetMemberImages(_selectedItem)).ContinueWith(prevTask =>
            {
                _searching = false;

                if (string.IsNullOrEmpty(MobileService.Error))
                {
                    if (prevTask.Result != null)
                    {
                        _selectedItem.PhotoByte = prevTask.Result.PhotoByte;
                        _selectedItem.SignatureByte = prevTask.Result.SignatureByte;
                        var activity = new Intent(this, typeof(OPviewMember));
                        activity.PutExtra("Member", _selectedItem.XmlSerializeToString());
                        StartActivity(activity);
                    }
                }
                else
                    HandleError();

                bar.Visibility = ViewStates.Gone;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private async void ShowVoucherMemberDetails()
        {
            var bar = FindViewById<ProgressBar>(Resource.Id.vviewprogress);
            bar.Visibility = ViewStates.Visible;
            _searching = true;

            // Get member biometrics
            if (_selectedVoucher.Redeemer != null)
            {
                await Task.Run(() => MobileService.GetMemberImages(_selectedVoucher.Redeemer)).ContinueWith(prevTask =>
                {
                    _searching = false;

                    if (string.IsNullOrEmpty(MobileService.Error))
                    {
                        if (prevTask.Result != null)
                        {
                            _selectedVoucher.Redeemer.PhotoByte = prevTask.Result.PhotoByte;
                            _selectedVoucher.Redeemer.SignatureByte = prevTask.Result.SignatureByte;
                            var activity = new Intent(this, typeof(OPviewMember));
                            activity.PutExtra("Member", _selectedVoucher.Redeemer.XmlSerializeToString());
                            StartActivity(activity);
                        }
                    }
                    else
                        HandleError();

                    bar.Visibility = ViewStates.Gone;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        public async void SearchForVoucher()
        {
            var vouchernum = FindViewById<EditText>(Resource.Id.vnum).Text;
            FindViewById<LinearLayout>(Resource.Id.vsearchmsg).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.vsearchResults).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.opvoucherviewbtnlayout).Visibility = ViewStates.Gone;

            if (string.IsNullOrEmpty(vouchernum))
            {
                FindViewById<LinearLayout>(Resource.Id.vsearchmsg).Visibility = ViewStates.Visible;
            }
            else
            {
                var bar = FindViewById<ProgressBar>(Resource.Id.vsearchprogress);
                bar.Visibility = ViewStates.Visible;

                _searching = true; // Dont allow search again
                await Task.Run(() => MobileService.SearchForVoucher(vouchernum)).ContinueWith(prevTask =>
                {
                    _searching = false;

                    if (string.IsNullOrEmpty(MobileService.Error))
                    {
                        if (prevTask.Result == null)
                        {
                            FindViewById<LinearLayout>(Resource.Id.vsearchmsg).Visibility = ViewStates.Visible;
                        }
                        else
                        {
                            FindViewById<LinearLayout>(Resource.Id.vsearchResults).Visibility = ViewStates.Visible;
                            SetVoucherSearchResult(prevTask.Result);
                        }
                    }
                    else
                        HandleError();

                    bar.Visibility = ViewStates.Gone;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void SetVoucherSearchResult(Voucher voucher)
        {
            // Clear current values
            FindViewById<TextView>(Resource.Id.vfirstname).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vsurname).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vemailAddress).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vmobileNum).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vdateRedeemed).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vstatus).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vhmo).Text = string.Empty;
            FindViewById<TextView>(Resource.Id.vnum2).Text = string.Empty;

            if (!string.IsNullOrEmpty(MobileService.Error))
            {
                HandleError();
            }
            else
            {
                if (voucher.Redeemer != null)
                {
                    // show view button
                    FindViewById<LinearLayout>(Resource.Id.opvoucherviewbtnlayout).Visibility = ViewStates.Visible;

                    FindViewById<TextView>(Resource.Id.vfirstname).Text = voucher.Redeemer.Firstname;
                    FindViewById<TextView>(Resource.Id.vsurname).Text = voucher.Redeemer.Surname;
                    FindViewById<TextView>(Resource.Id.vemailAddress).Text = voucher.Redeemer.Email;
                    FindViewById<TextView>(Resource.Id.vmobileNum).Text = voucher.Redeemer.Phonenum;
                    FindViewById<TextView>(Resource.Id.vdateRedeemed).Text = voucher.DateRedeemed;
                }

                FindViewById<TextView>(Resource.Id.vstatus).Text = voucher.Status;
                FindViewById<TextView>(Resource.Id.vhmo).Text = voucher.Hmo;
                FindViewById<TextView>(Resource.Id.vnum2).Text = voucher.VoucherId.ToString();

                FindViewById<LinearLayout>(Resource.Id.vsearchmsg).Visibility = ViewStates.Gone;
                FindViewById<LinearLayout>(Resource.Id.vsearchResults).Visibility = ViewStates.Visible;

                _selectedVoucher = voucher;
            }
        }

        private async void SearchForMembers()
        {
            var bar = FindViewById<ProgressBar>(Resource.Id.searchprogress);
            var lv = (ListView)FindViewById(Resource.Id.opsearchresults);
            var tv = FindViewById<TextView>(Resource.Id.searchheader);
            var mLinearLayout = FindViewById<LinearLayout>(Resource.Id.searchbox);

            // demo users
            //            Person dami = new Person() { Firstname = "Dami", Surname = "Lawal", Dob = "23/07/1980", NHISnum="40000002" };
            //            Person mayo = new Person() { Firstname = "Mayowa", Surname = "Kolawole", Dob = "05/02/1986", NHISnum = "40000055" };
            //            Person mina = new Person() { Firstname = "Mina", Surname = "Lawson", Dob = "11/11/1983", NHISnum = "40000752" };
            //
            //            if (dami.Firstname.ToLower().Contains(FindViewById<EditText>(Resource.Id.firstname).Text.ToLower()) || dami.Surname.ToLower().Contains(FindViewById<EditText>(Resource.Id.surname).Text.ToLower()))
            //            {
            //                result.Add(dami);
            //            }
            //
            //            if (mayo.Firstname.ToLower().Contains(FindViewById<EditText>(Resource.Id.firstname).Text.ToLower()) || mayo.Surname.ToLower().Contains(FindViewById<EditText>(Resource.Id.surname).Text.ToLower()))
            //            {
            //                result.Add(mayo);
            //            }
            //
            //            if (mina.Firstname.ToLower().Contains(FindViewById<EditText>(Resource.Id.firstname).Text.ToLower()) || mina.Surname.ToLower().Contains(FindViewById<EditText>(Resource.Id.surname).Text.ToLower()))
            //            {
            //                result.Add(mina);
            //            }

            // show progressbar
            bar.Visibility = ViewStates.Visible;

            // get search values
            var person = new Person();
            var personid = FindViewById<EditText>(Resource.Id.nhisnum).Text;
            if (!string.IsNullOrEmpty(personid))
                person.PersonId = int.Parse(FindViewById<EditText>(Resource.Id.nhisnum).Text);
            person.Firstname = FindViewById<EditText>(Resource.Id.firstname).Text.ToLower();
            person.Surname = FindViewById<EditText>(Resource.Id.surname).Text.ToLower();
            person.Email = FindViewById<EditText>(Resource.Id.email).Text.ToLower();
            person.Dob = FindViewById<EditText>(Resource.Id.dob).Text.ToLower();
            person.Phonenum = FindViewById<EditText>(Resource.Id.phonenum).Text.ToLower();
            person.Security = StringCipher.Encrypt(Helper.Operator.Username + "|" + Helper.Operator.Password, Helper.GetDateTimeNow());

            // verify something is searched for
            if (person.PersonId == 0 && string.IsNullOrEmpty(person.Firstname) && string.IsNullOrEmpty(person.Surname) && string.IsNullOrEmpty(person.Email)
                && string.IsNullOrEmpty(person.Dob) && string.IsNullOrEmpty(person.Phonenum))
            {
                var toast = Toast.MakeText(Application.Context, "Please fill at least one of the search boxes", ToastLength.Long);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                toast.Show();
                bar.Visibility = ViewStates.Gone;
            }
            else
            {
                _searching = true;
                await Task.Run(() => MobileService.SearchForMember(person)).ContinueWith(prevTask =>
                {
                    _searching = false;

                    if (prevTask.Result == null)
                    {
                        var toast = Toast.MakeText(Application.Context, "Error connecting to server", ToastLength.Long);
                        toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                        toast.Show();
                    }
                    else
                    {
                        if (prevTask.Result.Count == 0)
                        {
                            FindViewById<LinearLayout>(Resource.Id.searchresults).Visibility = ViewStates.Gone;
                            FindViewById<LinearLayout>(Resource.Id.searchmsg).Visibility = ViewStates.Visible;
                        }
                        else
                        {
                            CollapseLayout(mLinearLayout, tv);

                            var searchAdapter = new SearchAdapter(this, prevTask.Result);
                            lv.Adapter = searchAdapter;
                            FindViewById<LinearLayout>(Resource.Id.listViewDiv).LayoutParameters.Height = GetTotalHeightofListView(lv);
                            FindViewById<LinearLayout>(Resource.Id.searchmsg).Visibility = ViewStates.Gone;
                            FindViewById<LinearLayout>(Resource.Id.searchresults).Visibility = ViewStates.Visible;

                            tv.Text = "Search for another member";
                        }
                    }

                    // hide progressbar
                    bar.Visibility = ViewStates.Gone;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void HandleError()
        {
            if (MobileService.Error == "2")
            {
                var toast = Toast.MakeText(Application.Context, "Unauthorized Access", ToastLength.Long);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                toast.Show();
            }

            if (MobileService.Error == "3")
            {
                var toast = Toast.MakeText(Application.Context, "Invalid username or password", ToastLength.Long);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                toast.Show();
            }
        }

        private void CollapseLayout(LinearLayout mLinearLayout, TextView tv)
        {
            if (mLinearLayout.Visibility.Equals(ViewStates.Gone))
            {
                // set Visible
                tv.SetBackgroundResource(Resource.Drawable.topcorners);
                var paddingindp = (int)(30 / Resources.DisplayMetrics.Density);
                tv.SetPadding(paddingindp, paddingindp, paddingindp, paddingindp);

                mLinearLayout.Visibility = ViewStates.Visible;
                var widthSpec = View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                var heightSpec = View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                mLinearLayout.Measure(widthSpec, heightSpec);

                var mAnimator = Helper.SlideAnimator(0, mLinearLayout.MeasuredHeight, mLinearLayout);
                mAnimator.Start();
            }
            else
            {
                // set invisible
                var finalHeight = mLinearLayout.Height;

                var mAnimator = Helper.SlideAnimator(finalHeight, 0, mLinearLayout);
                mAnimator.Start();
                mAnimator.AnimationEnd += (intentSender, arg) =>
                {
                    mLinearLayout.Visibility = ViewStates.Gone;
                    tv.SetBackgroundResource(Resource.Drawable.allcorners);
                    var paddingindp = (int)(30 / Resources.DisplayMetrics.Density);
                    tv.SetPadding(paddingindp, paddingindp, paddingindp, paddingindp);
                };
            }
        }

        private void SetDrawer(Bundle bundle)
        {
            _title = _drawerTitle = Title;
            _menuTitles = new[] { "Web Account Details", "Member Search", "Voucher Search", "HMO Lookup", "Reset User Login", "Log Out" };
            _drawer = FindViewById<DrawerLayout>(Resource.Id.menulayout);
            _drawerList = FindViewById<ListView>(Resource.Id.left_drawer);

            _drawer.SetDrawerShadow(Resource.Drawable.drawer_shadow_dark, (int)GravityFlags.Start);

            _drawerList.Adapter = new ArrayAdapter<string>(this,
                Resource.Layout.DrawerListItem, _menuTitles);
            _drawerList.ItemClick += (sender, args) => SelectItem(args.Position);

            ActionBar.SetDisplayHomeAsUpEnabled(true);
            ActionBar.SetHomeButtonEnabled(true);

            //DrawerToggle is the animation that happens with the indicator next to the
            //ActionBar icon. You can choose not to use this.
            _drawerToggle = new MyActionBarDrawerToggle(this, _drawer,
                Resource.Drawable.ic_drawer_light,
                Resource.String.DrawerOpen,
                Resource.String.DrawerClose);

            //You can alternatively use _drawer.DrawerClosed here
            _drawerToggle.DrawerClosed += delegate
            {
                ActionBar.Title = _title;
                InvalidateOptionsMenu();
            };

            //You can alternatively use _drawer.DrawerOpened here
            _drawerToggle.DrawerOpened += delegate
            {
                ActionBar.Title = _drawerTitle;
                InvalidateOptionsMenu();
            };

            _drawer.SetDrawerListener(_drawerToggle);

            if (null == bundle)
                SelectItem(1);
        }

        private void SelectItem(int position)
        {
            // Reset all
            FindViewById<LinearLayout>(Resource.Id.opWAlayer).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.opESlayer).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.opLOPlayer).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.opVOPlayer).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.opHMOlayer).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.opResetlayer).Visibility = ViewStates.Gone;

            if (position == 0)
                FindViewById<LinearLayout>(Resource.Id.opWAlayer).Visibility = ViewStates.Visible;

            if (position == 1)
                FindViewById<LinearLayout>(Resource.Id.opESlayer).Visibility = ViewStates.Visible;

            if (position == 2)
                FindViewById<LinearLayout>(Resource.Id.opVOPlayer).Visibility = ViewStates.Visible;

            if (position == 3)
                FindViewById<LinearLayout>(Resource.Id.opHMOlayer).Visibility = ViewStates.Visible;

            if (position == 4)
                FindViewById<LinearLayout>(Resource.Id.opResetlayer).Visibility = ViewStates.Visible;

            if (position == 5)
                Finish();

            _drawerList.SetItemChecked(position, true);
            ActionBar.Title = _title = _menuTitles[position];
            _drawer.CloseDrawer(_drawerList);
        }

        public void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var listView = sender as ListView;
            var searchAdapter = listView?.Adapter as SearchAdapter;
            if (searchAdapter != null)
                _selectedItem = searchAdapter.GetPersonAtPosition(e.Position);
            ToggleButtons(true);
        }

        public void ToggleButtons(bool val)
        {
            FindViewById<Button>(Resource.Id.opviewbtn).Clickable = val;
            FindViewById<Button>(Resource.Id.opviewbtn).Alpha = val ? 1 : (float)0.25;
        }

        public int GetTotalHeightofListView(ListView listView)
        {
            var mAdapter = listView.Adapter as SearchAdapter;

            var totalHeight = 0;

            if (mAdapter != null)
            {
                for (var i = 0; i < mAdapter.Count; i++)
                {
                    var mView = mAdapter.GetView(i, null, listView);

                    mView.Measure(
                        View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified),

                        View.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified));

                    totalHeight += mView.MeasuredHeight;
                }
            }

            if (mAdapter != null)
                listView.LayoutParameters.Height = totalHeight + (listView.DividerHeight * (mAdapter.Count - 1));
            listView.RequestLayout();

            return listView.LayoutParameters.Height;
        }

        #endregion

        #region Activity Overrides

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            _drawerToggle.SyncState();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            _drawerToggle.OnConfigurationChanged(newConfig);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (_drawerToggle.OnOptionsItemSelected(item))
                return true;

            return base.OnOptionsItemSelected(item);
        }

        #endregion
    }
}

