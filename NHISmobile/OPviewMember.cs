﻿using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace NHISmobile
{
	[Activity (Label = "OPviewMember")]			
	public class OPviewMember : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			RequestWindowFeature(WindowFeatures.NoTitle);
			Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen); 

			SetContentView(Resource.Layout.OPviewMember);

			// Close button
			FindViewById<Button> (Resource.Id.opviewdone).Click += delegate {
				Finish();
			};

			// Set Values
			var text = Intent.GetStringExtra("Member") ?? string.Empty;

			if (!string.IsNullOrEmpty(text))
			{
				var member = text.XmlDeserializeFromString<Person>();
				FindViewById<TextView>(Resource.Id.opviewnhisnum).Text = member.PersonId.ToString();
				FindViewById<TextView>(Resource.Id.opviewfirstname).Text = member.Firstname;
				FindViewById<TextView>(Resource.Id.opviewsurname).Text = member.Surname;
				FindViewById<TextView>(Resource.Id.opviewmidname).Text = member.Middlename;
				FindViewById<TextView>(Resource.Id.opviewdob).Text = member.Dob;
				FindViewById<TextView>(Resource.Id.opviewemail).Text = member.Email;
				FindViewById<TextView>(Resource.Id.opviewgender).Text = member.Gender == "M" ? "Male" : member.Gender == "F" ? "Female" : member.Gender;
				FindViewById<TextView>(Resource.Id.opviewmarital).Text = member.Marital == "M" ? "Married" : member.Marital == "S" ? "Single" : member.Marital == "D" ? "Divorced" : member.Marital == "SP" ? "Single Parent" : member.Marital;
				FindViewById<TextView>(Resource.Id.opviewphone).Text = member.Phonenum;

				FindViewById<TextView>(Resource.Id.opviewhcp).Text = member.Hospital;
				FindViewById<TextView>(Resource.Id.opviewhmo).Text = member.Hmo;
				FindViewById<TextView>(Resource.Id.opviewcoverfrom).Text = member.CoverFrom;
				FindViewById<TextView>(Resource.Id.opviewcoverto).Text = member.CoverTo;

				if (member.PhotoByte != null) {
					var imgViewer = FindViewById<ImageView> (Resource.Id.opviewphotopic);
					SetImageFromArray (imgViewer, member.PhotoByte);
				}

				if (member.SignatureByte != null)
				{
					var imgViewer = FindViewById<ImageView> (Resource.Id.opviewsigpic);
					SetImageFromArray (imgViewer, member.SignatureByte);
				}
			}
		}

		private void SetImageFromArray(ImageView view, byte[] img)
		{
			var bm = BitmapFactory.DecodeByteArray(img, 0, img.Length);
			var dm = new DisplayMetrics();
			WindowManager.DefaultDisplay.GetMetrics (dm);

			view.SetMinimumHeight(dm.HeightPixels);
			view.SetMinimumWidth(dm.WidthPixels);
			view.SetImageBitmap(bm);
		}
	}
}

