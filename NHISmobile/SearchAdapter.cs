﻿using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;
using System.Collections.Generic;

namespace NHISmobile
{
    public class SearchAdapter : BaseAdapter
    {
        public readonly List<Person> ContactList;
        public readonly Activity Activity;

        public SearchAdapter(Activity activity, List<Person> contacts)
        {
            Activity = activity;
            ContactList = contacts;
        }

        public override int Count => ContactList.Count;

        public override Object GetItem(int position)
        {
            // could wrap a Contact in a Java.Lang.Object
            // to return it here if needed
            return null;
        }

        public override long GetItemId(int position)
        {
            return ContactList[position].Counter;
        }

        public Person GetPersonAtPosition(int position)
        {
            return ContactList[position];
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? Activity.LayoutInflater.Inflate(
                Resource.Layout.OPsearchItem, parent, false);

            view.FindViewById<TextView>(Resource.Id.searchfirstname).Text = ContactList[position].Firstname;
            view.FindViewById<TextView>(Resource.Id.searchsurname).Text = ContactList[position].Surname;
			view.FindViewById<TextView>(Resource.Id.searchnhisnum).Text = ContactList[position].PersonId.ToString();
            view.FindViewById<TextView>(Resource.Id.searchdob).Text = ContactList[position].Dob;

            return view;
        }

    }
}

