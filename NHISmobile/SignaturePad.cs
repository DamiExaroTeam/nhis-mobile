using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using SignaturePad;
using System.IO;

namespace NHISmobile
{
    [Activity(Label = "SignaturePad")]
    public class SignaturePad : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

            SetContentView(Resource.Layout.SignatureView);

            // Signature
            var signature = FindViewById<SignaturePadView>(Resource.Id.signatureView);

            // Activate this to internally use a bitmap to store the strokes
            // (good for frequent-redraw situations, bad for memory footprint)
            // signature.UseBitmapBuffer = true;

            signature.Caption.Text = "Sign here";
            signature.Caption.SetTypeface(Typeface.Serif, TypefaceStyle.BoldItalic);
            signature.Caption.SetTextSize(ComplexUnitType.Sp, 16f);
            signature.SignaturePrompt.Text = "";
            signature.SignaturePrompt.SetTypeface(Typeface.SansSerif, TypefaceStyle.Normal);
            signature.SignaturePrompt.SetTextSize(ComplexUnitType.Sp, 32f);

            signature.StrokeColor = Color.Black;
            signature.SetPadding(30, 30, 30, 30);
            signature.ClearLabel.SetBackgroundResource(Resource.Drawable.clearbtn);
            signature.ClearLabel.SetTextColor(Color.ParseColor("#1167a4"));
            signature.BackgroundColor = Color.Rgb(255, 255, 200);
            signature.SetBackgroundResource(Resource.Drawable.shadow);

            //signature.BackgroundImageView.SetImageResource (Resource.Drawable.logo_galaxy_black_64);
            //signature.BackgroundImageView.SetAlpha (16);
            //signature.BackgroundImageView.SetAdjustViewBounds (true);
			var layout = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            layout.AddRule(LayoutRules.CenterInParent);
            layout.SetMargins(20, 20, 0, 20);
            signature.BackgroundImageView.LayoutParameters = layout;

            // You can change paddings for positioning...
            var caption = signature.Caption;
            caption.SetPadding(caption.PaddingLeft, 1, caption.PaddingRight, 25);

            var save = FindViewById<Button>(Resource.Id.btnSigsave);
            save.Click += delegate
            {
				using (var os = new FileStream(App.Sigfile.Path, FileMode.Create))
				{
				    var sigimg = signature.GetImage();
				    sigimg.Compress(Bitmap.CompressFormat.Png, 95, os);
				}

                SetResult(Result.Ok); 
                Finish();
            };

            var cancel = FindViewById<Button>(Resource.Id.btnSigcancel);
			cancel.Click += delegate
            {
                SetResult(Result.Canceled);
                Finish();
            };
        }
    }
}