﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Text;
using Android.Views;
using Android.Widget;
using Com.Afisteam.Fpscanlib;
using Java.Lang;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VCmobileEntities;
using Environment = Android.OS.Environment;
using Exception = System.Exception;
using File = Java.IO.File;
using State = VCmobileEntities.State;
using String = System.String;
using Uri = Android.Net.Uri;

namespace NHISmobile
{
    [Activity(Label = "VCregistration", Theme = "@android:style/Theme.Holo.Light", ScreenOrientation=ScreenOrientation.Portrait)]			
	public class VCregistration : Activity
	{
        #region Variables

        private DateTime _dateHandle;
        private EditText _editHandle;
        private int _currTab = 1;
        private Person _currPerson;
        const int DateDialogId = 0;

        #endregion

        #region Constructor

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

			SetContentView (Resource.Layout.VCregistration);

            JavaSystem.LoadLibrary(@"usb-1.0");
            JavaSystem.LoadLibrary(@"ftrScanAPI");
            JavaSystem.LoadLibrary(@"ftrScanApiAndroidJni");
            JavaSystem.LoadLibrary(@"ftrWSQAndroid");
            JavaSystem.LoadLibrary(@"ftrWSQAndroidJni");

            // populate all dropdown boxes
            SetSpinners();

            // catch all datepicker clicks
            SetDatePickers();

            // create button events
            SetButtons();

            // add listeners to textboxes for validation
            AddValidation();

            // miscellenouse stuff
            SetMisc();

            // show intro message
            FadeInMessage();
		}

        #endregion

        #region Operations

        private void DoPrepopulation()
        {
            var text = Intent.GetStringExtra("SavedReg") ?? string.Empty;

            if (!string.IsNullOrEmpty(text))
            {
                _currPerson = text.XmlDeserializeFromString<Person>();

                // personal details
                SetVcRegistrationData(_currPerson);
                var tab = 2;

                if (_currPerson.BioDone == 1)
                {
                    var view = FindViewById<ImageView>(Resource.Id.fingeprintpic);
                    view.SetImageBitmap(BitmapFactory.DecodeFile(_currPerson.Fingerprint));
                    view.SetScaleType(ImageView.ScaleType.CenterCrop);

                    view = FindViewById<ImageView>(Resource.Id.photopic);
                    view.SetImageBitmap(BitmapFactory.DecodeFile(_currPerson.Photo));
                    view.SetScaleType(ImageView.ScaleType.CenterCrop);

                    view = FindViewById<ImageView>(Resource.Id.sigpic);
                    view.SetImageBitmap(BitmapFactory.DecodeFile(_currPerson.Signature));

                    tab = 1;
                }

                // Login details
                if (_currPerson.LoginDone == 1)
                {
                    FindViewById<EditText>(Resource.Id.username).Text = _currPerson.Username;
                    FindViewById<EditText>(Resource.Id.password).Text = _currPerson.Password;
                    FindViewById<EditText>(Resource.Id.verifypassword).Text = _currPerson.Password;
                    tab = 3;
                }

                // Hospital
                if (_currPerson.HospitalDone == 1)
                {
                    var cmbstate = FindViewById<Spinner>(Resource.Id.cmbstate);
                    var cmblga = FindViewById<Spinner>(Resource.Id.cmblga);
                    var cmbhcp = FindViewById<Spinner>(Resource.Id.cmbHCP);
                    cmbstate.SetSelection(GetSpinnerIndex(cmbstate, _currPerson.State.StateName));
                    cmblga.SetSelection(GetSpinnerIndex(cmblga, _currPerson.State.SelectedLga.LgAname));
                    cmblga.SetSelection(GetSpinnerIndex(cmbhcp, _currPerson.State.SelectedLga.SelectedHospital));
                    tab = 4;
                }

                // Voucher
                if (_currPerson.VoucherDone == 1)
                {
                    FindViewById<EditText>(Resource.Id.vouchernum).Text = _currPerson.VoucherNumber.ToString();
                    FindViewById<EditText>(Resource.Id.vpin1).Text = _currPerson.VoucherPin.Substring(0, 4);
                    FindViewById<EditText>(Resource.Id.vpin2).Text = _currPerson.VoucherPin.Substring(4, 4);
                    FindViewById<EditText>(Resource.Id.vpin3).Text = _currPerson.VoucherPin.Substring(8, 4);
                    FindViewById<EditText>(Resource.Id.vpin4).Text = _currPerson.VoucherPin.Substring(12, 4);
                    FindViewById<EditText>(Resource.Id.verifypassword).Text = _currPerson.Password;
                    tab = 5;
                }

                SetTab(tab);
            }
            else
                SetTab(1);
        }

        public async void FadeInMessage()
        {
            var intro = FindViewById<RelativeLayout>(Resource.Id.intromsg);

            await Task.Delay(1000);
            intro.Visibility = ViewStates.Visible;
            await Task.Delay(3300);
            intro.Visibility = ViewStates.Gone;

            // Fade in registration form
            var form = FindViewById<RelativeLayout>(Resource.Id.regform);
            form.Visibility = ViewStates.Visible;

            // Prepopulate data if its an edit
            DoPrepopulation();
        }

        public void ResetAllTabs()
        {
            for (var i = 0; i < 6; i++)
            {
                var tid = i == 0 ? Resource.Id.pinfotab : i == 1 ? Resource.Id.logintab : i == 2 ? Resource.Id.hcptab : i == 3 ? Resource.Id.vtab : Resource.Id.biotab;
                var lid = i == 0 ? Resource.Id.pdlayer : i == 1 ? Resource.Id.loginlayer : i == 2 ? Resource.Id.hospitallayer : i == 3 ? Resource.Id.voucherlayer : Resource.Id.biometricslayer;
                var imgid = i == 0 ? Resource.Id.pinfotabimg : i == 1 ? Resource.Id.logintabimg : i == 2 ? Resource.Id.hcptabimg : i == 3 ? Resource.Id.vtabimg : Resource.Id.biotabimg;

                var tv = FindViewById<TextView>(tid);
                var imgview = FindViewById<ImageView>(imgid);
                imgview.SetImageResource(Resources.GetIdentifier("tabedgeclear", "drawable", PackageName));
                imgview?.SetAlpha(66);
                tv.SetTextColor(Color.ParseColor("#ff1167a4"));
                tv.SetBackgroundColor(Color.ParseColor("#00000000"));

                var layout = FindViewById<LinearLayout>(lid);
                layout.Visibility = ViewStates.Gone;
            }
        }

        public void SetDatePickers()
        {
            _editHandle = FindViewById<EditText>(Resource.Id.dob);
            _editHandle.Click += delegate
            {
                ShowDialog(DateDialogId);
            };

            // Set default date on calender
            _dateHandle = DateTime.Today;

            // Set default textbox message
            _editHandle.Text = "Select date";
        }

        public void SetSpinners()
        {
            // Gender
            var spinner = FindViewById<Spinner>(Resource.Id.cmbgender);
            String[] genderArray = { "Select gender","Male","Female" };
            spinner.ItemSelected += delegate(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var adapterView = sender as AdapterView;
                ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
            };

            var adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, genderArray);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            // Marital status
            spinner = FindViewById<Spinner>(Resource.Id.cmbmarital);
            String[] maritalArray = { "Select status", "Married", "Single", "Divorced", "Single Parent" };
            spinner.ItemSelected += delegate(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var adapterView = sender as AdapterView;
                ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
            };
            adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, maritalArray);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            // States
            spinner = FindViewById<Spinner>(Resource.Id.cmbstate);
            spinner.ItemSelected += delegate(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var adapterView = sender as AdapterView;
                ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
            };
            var hcpspinner = FindViewById<Spinner>(Resource.Id.cmbHCP);
            hcpspinner.ItemSelected += delegate(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var adapterView = sender as AdapterView;
                ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
            };
            var lgaspinner = FindViewById<Spinner>(Resource.Id.cmblga);
            lgaspinner.ItemSelected += delegate(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                var adapterView = sender as AdapterView;
                ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
            };
            var states = GetTempStates();
            List<Lga> lgas;
            List<string> hcps = null;
            var lgaprepop = false;
            var hcpprepop = false;
            adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, states);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;
            spinner.ItemSelected += delegate
            {
                var val = spinner.SelectedItem.ToString();

                foreach (var s in states)
                {
                    // set lgas
                    if (s.StateName == val && val != "Select a state")
                    {
                        FindViewById<EditText>(Resource.Id.stateerr).Error = null; // reset errorbox
                        lgas = s.LgAs;
                        lgaspinner.Clickable = true; lgaspinner.Alpha = 1; // activate dropdown
                        var lgaadapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, lgas);
                        lgaadapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                        lgaspinner.Adapter = lgaadapter;
                        lgaspinner.ItemSelected += delegate
                        {
                            var lgaval = lgaspinner.SelectedItem.ToString();

                            foreach (var l in lgas)
                            {
                                // set hospitals
                                if (l.LgAname == lgaval && lgaval != "Select an LGA")
                                {
                                    FindViewById<EditText>(Resource.Id.lgaerr).Error = null; // reset errorbox
                                    hcps = l.Hospitals;
                                    hcpspinner.Clickable = true; hcpspinner.Alpha = 1; // activate dropdown
                                    var hcpadapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerItem, hcps);
                                    hcpadapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                                    hcpspinner.Adapter = hcpadapter;

                                    // hcp prepopulation
                                    if (_currPerson != null && _currPerson.State != null && _currPerson.State.SelectedLga != null && !string.IsNullOrEmpty(_currPerson.State.SelectedLga.SelectedHospital) && !hcpprepop)
                                    {
                                        hcpspinner.SetSelection(GetSpinnerIndex(hcpspinner, _currPerson.State.SelectedLga.SelectedHospital));
                                        hcpprepop = true;
                                    }

                                    break;
                                }
                                hcpspinner.Clickable = false; hcpspinner.Alpha = (float)0.25;
                            }
                        };

                        // lga prepopulation
                        if (_currPerson != null && _currPerson.State != null && _currPerson.State.SelectedLga != null && !lgaprepop)
                        {
                            lgaspinner.SetSelection(GetSpinnerIndex(lgaspinner, _currPerson.State.SelectedLga.LgAname));
                            lgaprepop = true;
                        }

                        break;
                    }
                    //deactivate them again
                    hcpspinner.Clickable = false; hcpspinner.Alpha = (float)0.25;
                    lgaspinner.Clickable = false; lgaspinner.Alpha = (float)0.25;
                }
            };
        }

        public List<State> GetTempStates()
        {
            var states = new List<State>();

            states.Add(new State { StateName = "Select a state" });

            var lagos = new State();
            lagos.StateName = "Lagos";
            lagos.LgAs = new List<Lga>
            {
				new Lga { LgAname = "Select an LGA" },
				new Lga
				{
					LgAname = "Agege",
					Hospitals = new List<string>
					{
						"Select a hospital",
						"All Souls Infirmary Hospital & Mat. Centre",
						"Ancilla Catholic Hospital",
						"Aqua Libra Clinic and Maternity",
						"Talent Specialist Hospital"
					}
				},
				new Lga
				{
					LgAname = "Lagos Island",
					Hospitals = new List<string>
					{
						"Select a hospital",
						"ANNA MARIA HOSPITAL",
						"Gold Cross Hospital",
						"Lagos State General Hospital",
						"The Health Arena"
					}
				},
				new Lga
				{
					LgAname = "Lagos Mainland",
					Hospitals = new List<string>
					{
						"Select a hospital",
						"Acme Medical Centre",
						"Marien Clinic",
						"Nigerian Railway Hospital"
					}
				}
			};

            states.Add(lagos);

            var fct = new State();
            fct.StateName = "FCT";
            fct.LgAs = new List<Lga>
            {
				new Lga { LgAname = "Select an LGA" },
				new Lga
				{
					LgAname = "Abaji",
					Hospitals = new List<string>
					{
						"Select a hospital",
						"FCT/0104 - Abaji General Hospital",
						"FCT/0152 - Lafiak Hospital",
						"FCT/0476 - St. Peters Catholic Hospital"
					}
				},
				new Lga
				{
					LgAname = "Kwali",
					Hospitals = new List<string>
					{
						"Select a hospital",
						"Kwali General Hospital",
						"Rhema Foundation Hospital"
					}
				},
				new Lga
				{
					LgAname = "Municipal",
					Hospitals = new List<string>
					{
						"Select a hospital",
						"Abuja Clinic - Maitama",
						"Arewa Clinic & Maternity - Karu",
						"Cornerstone Specialist Hospital",
						"Kings Care Hospital - Wuse",
						"Sauki Private Hospital",
						"Wuse District Hospital",
						"Zenith Medical and Kidney Centre"
					}
				}
			};

            states.Add(fct);

            return states;
        }

        public void SetButtons()
        {
            var backbtn = FindViewById<Button>(Resource.Id.bkbtn);
            backbtn.Click += delegate
            {
                _currTab--;
                SetTab(_currTab);
            };

            try
            {
                string msg;
                // Save and continue event
                var savebtn = FindViewById<Button>(Resource.Id.savebtn);
                savebtn.Click += delegate
                {
                    if (_currTab == 1)
                    {
                        // Check and Save current data
                        if (ValidatePersonalInfo())
                        {
                            var newperson = new Person
                            {
                                Firstname = FindViewById<EditText>(Resource.Id.firstname).Text,
                                Surname = FindViewById<EditText>(Resource.Id.surname).Text,
                                Phonenum = FindViewById<EditText>(Resource.Id.phonenum).Text,
                                Email = FindViewById<EditText>(Resource.Id.email).Text,
                                Dob = FindViewById<EditText>(Resource.Id.dob).Text,
                                Middlename = FindViewById<EditText>(Resource.Id.midname).Text,
                                NatId = FindViewById<EditText>(Resource.Id.natid).Text,
                                Gender = FindViewById<Spinner>(Resource.Id.cmbgender).SelectedItem.ToString(),
                                Marital = FindViewById<Spinner>(Resource.Id.cmbmarital).SelectedItem.ToString()
                            };

                            if (_currPerson == null)
                            {
                                _currPerson = newperson;

                                // Save to database
                                _currPerson.PersonId = DataCenter.SaveVcRegisterProfile(_currPerson);
                                msg = "Personal data saved";
                            }
                            else
                            {
                                // Update
                                newperson.PersonId = _currPerson.PersonId;
                                _currPerson = newperson;
                                DataCenter.UpdateVcRegisterProfile(_currPerson);
                                msg = "Personal data updated";
                            }

                            // Give little message
                            var toast = Toast.MakeText(Application.Context, msg, ToastLength.Short);
                            toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 10);
                            toast.Show();

                            _currTab++;
                            SetTab(_currTab);
                        }
                    }

                    else if (_currTab == 2)
                    {
                        // Check and Save current data
                        if (ValidateUsername())
                        {
                            _currPerson.Username = FindViewById<EditText>(Resource.Id.username).Text;
                            _currPerson.Password = FindViewById<EditText>(Resource.Id.password).Text;

                            // Save to database
                            _currPerson.LoginDone = 1;
                            DataCenter.SaveVcUsername(_currPerson);

                            // Give little message
                            var toast = Toast.MakeText(Application.Context, "Login details updated", ToastLength.Short);
                            toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 10);
                            toast.Show();

                            _currTab++;
                            SetTab(_currTab);
                        }
                    }

                    else if (_currTab == 3)
                    {
                        // Check and Save current data
                        if (ValidateHospital())
                        {
                            FindViewById<EditText>(Resource.Id.hospitalerr).Error = null; // reset errorbox
                            _currPerson.State = new State
                            {
                                StateName = FindViewById<Spinner>(Resource.Id.cmbstate).SelectedItem.ToString(),
                                SelectedLga = new Lga
                                {
                                    LgAname = FindViewById<Spinner>(Resource.Id.cmblga).SelectedItem.ToString(),
                                    SelectedHospital = FindViewById<Spinner>(Resource.Id.cmbHCP).SelectedItem.ToString()
                                }
                            };

                            // Save to database
                            _currPerson.HospitalDone = 1;
                            DataCenter.SaveVcHospital(_currPerson);

                            // Give little message
                            var toast = Toast.MakeText(Application.Context, "HCP details updated", ToastLength.Short);
                            toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 10);
                            toast.Show();

                            _currTab++;
                            SetTab(_currTab);
                        }
                    }

                    else if (_currTab == 4)
                    {
                        // Check and Save current data
                        if (ValidateVoucher())
                        {
                            _currPerson.VoucherNumber = int.Parse(FindViewById<EditText>(Resource.Id.vouchernum).Text);
                            _currPerson.VoucherPin = FindViewById<EditText>(Resource.Id.vpin1).Text;
                            //currPerson.VoucherPIN = (FindViewById<EditText> (Resource.Id.vpin1) as EditText).Text + "-" + (FindViewById<EditText> (Resource.Id.vpin2) as EditText).Text + "-" +
                            //	(FindViewById<EditText> (Resource.Id.vpin3) as EditText).Text + "-" + (FindViewById<EditText> (Resource.Id.vpin4) as EditText).Text + "-" + (FindViewById<EditText> (Resource.Id.vpin5) as EditText).Text;

                            // Save to database
                            _currPerson.VoucherDone = 1;
                            DataCenter.SaveVcVoucher(_currPerson);

                            // Give little message
                            var toast = Toast.MakeText(Application.Context, "Voucher details updated", ToastLength.Short);
                            toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 10);
                            toast.Show();

                            _currTab++;
                            SetTab(_currTab);
                        }
                    }

                    else if (_currTab == 5)
                    {
                        msg = string.Empty;

                        if (string.IsNullOrEmpty(_currPerson.Photo))
                            msg = "Please capture photograph";

                        if (string.IsNullOrEmpty(_currPerson.Fingerprint))
                            msg = "Please capture fingerprint";

                        if (string.IsNullOrEmpty(_currPerson.Signature))
                            msg = "Please capture signature";

                        if (msg == string.Empty)
                        {
                            msg = "Biometrics saved";
                            _currPerson.BioDone = 1;
                            DataCenter.UpdateVcRegisterProfile(_currPerson);
                            var toast = Toast.MakeText(Application.Context, msg, ToastLength.Short);
                            toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 20);
                            toast.Show();
                            Finish();
							StartActivity(typeof(VCsavedReg));
                        }
                        else
                        {
                            var toast = Toast.MakeText(Application.Context, msg, ToastLength.Short);
                            toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Center, 0, 0);
                            toast.Show();
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                // Show error message
                var toast = Toast.MakeText(Application.Context, ex.Message, ToastLength.Long);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 10);
                toast.Show();
            }

            // Biometrics buttons
            var fp = FindViewById<ImageView>(Resource.Id.fingeprintpic);
            fp.Click += delegate
            {
                CreateDirectoryForFingerprints();

                var fingerprintIntent = new Intent().SetClass(this, typeof(FpScanActivity));
                StartActivityForResult(fingerprintIntent, 2);
                SetResult(Result.Ok, fingerprintIntent);
            };

            var pic = FindViewById<ImageView>(Resource.Id.photopic);
            var intent = new Intent(MediaStore.ActionImageCapture);
            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();

                if (App.Bitmap != null)
                {
                    pic.SetImageBitmap(App.Bitmap);
                    App.Bitmap = null;
                }

                pic.Click += delegate
                {
                    App._file = new File(App.Dir, String.Format(_currPerson.Surname + "_" + _currPerson.PersonId + ".jpg", Guid.NewGuid()));

                    intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(App._file));

                    StartActivityForResult(intent, 1);
                };
            }
            else
            {
                // Give little message
                var toast = Toast.MakeText(Application.Context, "No camera app found on this device", ToastLength.Short);
                toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, 10);
                toast.Show();
            }

            var sig = FindViewById<ImageView>(Resource.Id.sigpic);
            var sigintent = new Intent().SetClass(this, typeof(SignaturePad));
            sig.Click += delegate
            {
                CreateDirectoryForSignatures();

                App.Sigfile = new File(App.Sigdir, String.Format(_currPerson.Surname + "_" + _currPerson.PersonId + ".jpg", Guid.NewGuid()));
                StartActivityForResult(sigintent, 3);
            };

            // Set tabs for when they are clicked on
            var tv = FindViewById<TextView>(Resource.Id.pinfotab);
            tv.Click += delegate
            {
                _currTab = 1;
                SetTab(_currTab);
            };

            tv = FindViewById<TextView>(Resource.Id.logintab);
            tv.Click += delegate
            {
                if (_currPerson != null)
                {
                    _currTab = 2;
                    SetTab(_currTab);
                }
            };

            tv = FindViewById<TextView>(Resource.Id.hcptab);
            tv.Click += delegate
            {
                if (_currPerson != null && _currPerson.LoginDone == 1)
                {
                    _currTab = 3;
                    SetTab(_currTab);
                }
            };

            tv = FindViewById<TextView>(Resource.Id.vtab);
            tv.Click += delegate
            {
                if (_currPerson != null && _currPerson.HospitalDone == 1)
                {
                    _currTab = 4;
                    SetTab(_currTab);
                }
            };

            tv = FindViewById<TextView>(Resource.Id.biotab);
            tv.Click += delegate
            {
                if (_currPerson != null && _currPerson.VoucherDone == 1)
                {
                    _currTab = 5;
                    SetTab(_currTab);
                }
            };
        }

        private bool IsThereAnAppToTakePictures()
        {
            var intent = new Intent(MediaStore.ActionImageCapture);
            var availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

        private void CreateDirectoryForPictures()
        {
            App.Dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "VC Registration Photos");
            if (!App.Dir.Exists())
            {
                App.Dir.Mkdirs();
            }
        }

        private void CreateDirectoryForSignatures()
        {
            App.Sigdir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "VC Registration Signatures");
            if (!App.Sigdir.Exists())
            {
                App.Sigdir.Mkdirs();
            }
        }

        private void CreateDirectoryForFingerprints()
        {
            App.Findir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "VC Registration Fingerprints");
            if (!App.Findir.Exists())
            {
                App.Findir.Mkdirs();
            }
        }

        public void SetMisc()
        {
            // Set intro text for username creation
            var intro = FindViewById<TextView>(Resource.Id.userhelptxt);
            intro.Text = Html.FromHtml(@"<div>Create an account username and password</div>
            &#8226; Your password must be between 6 and 16 characters and cannot be 'password'.<br/><br/>
            &#8226; Your password must have letters and numbers and also at least ONE capital letter.<br/><br/>
            &#8226; Remember your password is case sensitive!<br/>").ToString();

            // Change size of biometrics based on the screen size
            if (Resources.Configuration.ScreenWidthDp < 600)
            {
                var view = FindViewById<ImageView>(Resource.Id.photopic);
                view.LayoutParameters.Height = ConvertDpToPixels(210);
                view.LayoutParameters.Width = ConvertDpToPixels(160);
                view.RequestLayout();

                view = FindViewById<ImageView>(Resource.Id.fingeprintpic);
                view.LayoutParameters.Height = ConvertDpToPixels(210);
				view.LayoutParameters.Width = ConvertDpToPixels(110);
                view.RequestLayout();

                view = FindViewById<ImageView>(Resource.Id.sigpic);
				view.LayoutParameters.Height = ConvertDpToPixels(110);
                view.LayoutParameters.Width = ConvertDpToPixels(300);
                view.RequestLayout();

            }
        }

        private int ConvertPixelsToDp(int pixelValue)
        {
            return (int)((pixelValue) / Resources.DisplayMetrics.Density);
        }

        private int ConvertDpToPixels(int dpValue)
        {
            return (int)(Resources.DisplayMetrics.Density * dpValue);
        }

        public void SetTab(int val)
        {
            TextView tv = null;
            LinearLayout layer = null;
            ImageView imgv1 = null;
            ImageView imgv2 = null;
            _currTab = val;

            ResetAllTabs();

            // show back button for all tabs except first
            FindViewById<Button>(Resource.Id.bkbtn).Visibility = val == 1 ? ViewStates.Gone : ViewStates.Visible;

            FindViewById<Button>(Resource.Id.savebtn).Visibility = ViewStates.Visible;

            if (val == 1)
            {
                layer = FindViewById<LinearLayout>(Resource.Id.pdlayer);
                tv = FindViewById<TextView>(Resource.Id.pinfotab);
                imgv1 = FindViewById<ImageView>(Resource.Id.pinfotabimg);
            }

            if (val == 2)
            {
                layer = FindViewById<LinearLayout>(Resource.Id.loginlayer);
                tv = FindViewById<TextView>(Resource.Id.logintab);
                imgv2 = FindViewById<ImageView>(Resource.Id.pinfotabimg);
                imgv1 = FindViewById<ImageView>(Resource.Id.logintabimg);
            }

            if (val == 3)
            {
                layer = FindViewById<LinearLayout>(Resource.Id.hospitallayer);
                tv = FindViewById<TextView>(Resource.Id.hcptab);
                imgv2 = FindViewById<ImageView>(Resource.Id.logintabimg);
                imgv1 = FindViewById<ImageView>(Resource.Id.hcptabimg);
            }

            if (val == 4)
            {
                layer = FindViewById<LinearLayout>(Resource.Id.voucherlayer);
                tv = FindViewById<TextView>(Resource.Id.vtab);
                imgv2 = FindViewById<ImageView>(Resource.Id.hcptabimg);
                imgv1 = FindViewById<ImageView>(Resource.Id.vtabimg);
            }

            if (val == 5)
            {
                layer = FindViewById<LinearLayout>(Resource.Id.biometricslayer);
                tv = FindViewById<TextView>(Resource.Id.biotab);
                imgv2 = FindViewById<ImageView>(Resource.Id.vtabimg);
                imgv1 = FindViewById<ImageView>(Resource.Id.biotabimg);
            }

            if (layer != null) layer.Visibility = ViewStates.Visible;
            if (tv != null)
            {
                tv.SetTextColor(Color.ParseColor("#ffffff"));
                tv.SetBackgroundColor(Color.ParseColor("#1167a4"));
            }

            if (imgv1 != null)
            {
                imgv1.SetImageResource(Resources.GetIdentifier("tabedge", "drawable", PackageName));
                imgv1.SetAlpha(255);
            }

            if (imgv2 != null)
            {
                imgv2.SetImageResource(Resources.GetIdentifier("tabedgex", "drawable", PackageName));
                imgv2.SetAlpha(255);
            }

            // Scroll to top
            var sv = FindViewById<ScrollView>(Resource.Id.vcregscroller);
            sv.ScrollTo(0, sv.Bottom);
        }

        public void AddValidation()
        {
            var icon = Resources.GetDrawable(Resources.GetIdentifier("icon", "drawable", PackageName));
            var textboxes = new List<EditText>();
            textboxes.Add(FindViewById<EditText>(Resource.Id.surname));
            textboxes.Add(FindViewById<EditText>(Resource.Id.firstname));
            textboxes.Add(FindViewById<EditText>(Resource.Id.phonenum));
            textboxes.Add(FindViewById<EditText>(Resource.Id.email));
            textboxes.Add(FindViewById<EditText>(Resource.Id.dob));

            foreach (var textbox in textboxes)
            {
                textbox.FocusChange += (sender, args) =>
                {
                    var editText = sender as EditText;
                    if (editText != null && (Resources.GetResourceName(editText.Id).Contains("phonenum") && 
                    editText.Text.Length != 11 && editText.Text.Length != 0))
                    {
                        ((EditText) sender).SetError("A mobile number should have 11 digits", icon);
                    }
                    else
                    {
                        var text = sender as EditText;
                        if (text != null && text.Text != string.Empty)
                        {
                            ((EditText) sender).Background.SetColorFilter(Color.White, PorterDuff.Mode.Multiply);
                        }
                    }
                };

            }
        }

        public bool ValidatePersonalInfo()
        {
            var result = true;
            var hasfocused = false;
            var icon = Resources.GetDrawable(Resources.GetIdentifier("icon", "drawable", PackageName));

            // Check text boxes
            var textboxes = new List<EditText>();
            textboxes.Add(FindViewById<EditText>(Resource.Id.surname));
            textboxes.Add(FindViewById<EditText>(Resource.Id.firstname));
            textboxes.Add(FindViewById<EditText>(Resource.Id.dob));
            textboxes.Add(FindViewById<EditText>(Resource.Id.phonenum));

            foreach (var text in textboxes)
            {
                var name = Resources.GetResourceName(text.Id).Substring(Resources.GetResourceName(text.Id).IndexOf(":id/") + ":id/".Length);
                name = name == "phonenum" ? "mobile phone number" : name == "dob" ? "date of birth" : name;

                if (text.Text == string.Empty || text.Text == "Select date" || (name == "mobile phone number" && text.Text.Length != 11))
                {
                    text.SetError(name == "phonenum" ? "A mobile number should have 11 digits" : "Please enter your " + name, icon);
                    SetBGcolor(text, hasfocused, true);
                    hasfocused = true;
                    result = false;
                }
                else
                {
                    SetBGcolor(text, hasfocused, false);
                }
            }

            // Check spinners
            var spinners = new List<Spinner>
            {
                FindViewById<Spinner>(Resource.Id.cmbgender),
                FindViewById<Spinner>(Resource.Id.cmbmarital)
            };

            foreach (var spinner in spinners)
            {
                var name = Resources.GetResourceName(spinner.Id).Substring(Resources.GetResourceName(spinner.Id).IndexOf(":id/") + ":id/".Length);

                EditText spinnertext;
                if (name == "cmbgender")
                {
                    name = "gender";
                    spinnertext = FindViewById<EditText>(Resource.Id.gendererr);
                }
                else
                {
                    name = "marital status";
                    spinnertext = FindViewById<EditText>(Resource.Id.maritalerr);
                }

                if (spinner.SelectedItem == null)
                {
                    spinnertext.SetError("Please select your " + name, icon);
                    SetBGcolor(spinnertext, hasfocused, true);
                    hasfocused = true;
                    result = false;
                }
                else
                {
                    SetBGcolor(spinnertext, hasfocused, false);
                }
            }

            return result;
        }

        public bool ValidateUsername()
        {
            var result = true;
            var isvalid = true;
            var hasfocused = false;
            var icon = Resources.GetDrawable(Resources.GetIdentifier("icon", "drawable", PackageName));

            // Check text boxes
            var textboxes = new List<EditText>
            {
                FindViewById<EditText>(Resource.Id.username),
                FindViewById<EditText>(Resource.Id.password),
                FindViewById<EditText>(Resource.Id.verifypassword)
            };

            foreach (var text in textboxes)
            {
                // Reset first
                SetBGcolor(text, true, false);

                var name = Resources.GetResourceName(text.Id).Substring(Resources.GetResourceName(text.Id).IndexOf(":id/") + ":id/".Length);

                if (text.Text == string.Empty)
                {
                    if (name == "verifypassword")
                        text.SetError("Enter password again please", icon);
                    else
                        text.SetError("Please enter your " + name, icon);
                    SetBGcolor(text, hasfocused, true);
                    hasfocused = true;
                    result = false;
                }
                else
                {
                    if (name == "username" && (text.Text.Length < 3 || text.Text.Length > 20))
                    {
                        isvalid = false;
                        text.SetError("Username must be 3-20 characters", icon);
                    }

                    if (name == "password")
                    {
                        if (text.Text.Length < 6 || text.Text.Length > 16)
                        {
                            text.SetError("Password must be 3-20 characters", icon);
                            isvalid = false;
                        }

                        if (!text.Text.Any(char.IsUpper) || !text.Text.Any(char.IsDigit))
                        {
                            text.SetError("Password must have one capital letter and a number", icon);
                            isvalid = false;
                        }
                    }

                    if (name == "verifypassword")
                    {
                        if (text.Text != FindViewById<EditText>(Resource.Id.password).Text)
                        {
                            text.SetError("Password and Verify password must match", icon);
                            isvalid = false;
                        }
                    }

                    result = isvalid;
                    if (!result)
                    {
                        SetBGcolor(text, hasfocused, true);
                        hasfocused = true;
                    }
                }
            }

            return result;
        }

        public bool ValidateHospital()
        {
            var result = true;
            var hasfocused = false;
            var icon = Resources.GetDrawable(Resources.GetIdentifier("icon", "drawable", PackageName));
            var spinners = new List<Spinner>
            {
                FindViewById<Spinner>(Resource.Id.cmbstate),
                FindViewById<Spinner>(Resource.Id.cmblga),
                FindViewById<Spinner>(Resource.Id.cmbHCP)
            };

            foreach (var spinner in spinners)
            {
                var name = Resources.GetResourceName(spinner.Id).Substring(Resources.GetResourceName(spinner.Id).IndexOf(":id/") + ":id/".Length);

                EditText spinnertext = null;
                if (name == "cmbstate")
                {
                    name = "state";
                    spinnertext = FindViewById<EditText>(Resource.Id.stateerr);
                }
                else if (name == "cmblga")
                {
                    name = "LGA";
                    spinnertext = FindViewById<EditText>(Resource.Id.lgaerr);
                }
                else
                {
                    name = "Healthcare facility";
                    spinnertext = FindViewById<EditText>(Resource.Id.hospitalerr);
                }

                if (spinner.SelectedItem == null || spinner.SelectedItem.ToString().Contains("Select a"))
                {
                    spinnertext.SetError("Please select a " + name, icon);
                    SetBGcolor(spinnertext, hasfocused, true);
                    hasfocused = true;
                    result = false;
                }
                else
                {
                    SetBGcolor(spinnertext, hasfocused, false);
                }
            }

            return result;
        }

        public bool ValidateVoucher()
        {
            var result = true;
            var isvalid = true;
            var hasfocused = false;
            var icon = Resources.GetDrawable(Resources.GetIdentifier("icon", "drawable", PackageName));

            var tx = FindViewById<EditText>(Resource.Id.vouchernum);
            if (tx.Text == string.Empty)
            {
                tx.SetError("Please enter voucher number", icon);
                SetBGcolor(tx, hasfocused, true);
                hasfocused = true;
            }

            // Check text boxes
            var textboxes = new List<EditText>
            {
                FindViewById<EditText>(Resource.Id.vpin1),
                FindViewById<EditText>(Resource.Id.vpin2),
                FindViewById<EditText>(Resource.Id.vpin3),
                FindViewById<EditText>(Resource.Id.vpin4)
            };

            foreach (var text in textboxes)
            {
                // Reset first
                SetBGcolor(text, true, false);

                Resources.GetResourceName(text.Id).Substring(Resources.GetResourceName(text.Id).IndexOf(":id/") + ":id/".Length);

                if (text.Text == string.Empty)
                {
                    text.SetError("Please enter voucher PIN", icon);
                    SetBGcolor(text, hasfocused, true);
                    hasfocused = true;
                    result = false;
                }
                else
                {
                    if (text.Text.Length != 4)
                    {
                        isvalid = false;
                    }

                    result = isvalid;
                    if (!result)
                    {
                        text.SetError("Invalid PIN entered", icon);
                        SetBGcolor(text, hasfocused, true);
                        hasfocused = true;
                    }
                }
            }

            return result;
        }

        public bool VerifyBiometrics()
        {
            var result = true;
            //SignaturePadView signature = FindViewById<SignaturePadView>(Resource.Id.signatureView);
            //if (signature.IsBlank)
            //{
            //    // Give little message
            //    result = false;
            //    Toast toast = Toast.MakeText(Application.Context, "No signature found", ToastLength.Short);
            //    toast.SetGravity(GravityFlags.CenterHorizontal | GravityFlags.Bottom, 0, -20);
            //    toast.Show();
            //}
            //else
            //{
            //    // do something with image
            //    Bitmap bmp = signature.GetImage();
            //}

            return result;
        }

        public void SetBGcolor(EditText text, bool hasfocused, bool highlight)
        {
            if (highlight)
            {
                text.Background.SetColorFilter(Color.Pink, PorterDuff.Mode.Multiply);
                if (!hasfocused)
                {
                    text.RequestFocus();
                }
            }
            else
            {
                text.Background.SetColorFilter(Color.White, PorterDuff.Mode.Multiply);
                text.Error = null;
            }
        }

        public void SetVcRegistrationData(Person person)
        {
            FindViewById<EditText>(Resource.Id.firstname).Text = person.Firstname;
            FindViewById<EditText>(Resource.Id.surname).Text = person.Surname;
            FindViewById<EditText>(Resource.Id.phonenum).Text = person.Phonenum;
            FindViewById<EditText>(Resource.Id.email).Text = person.Email;
            FindViewById<EditText>(Resource.Id.dob).Text = person.Dob;
            FindViewById<EditText>(Resource.Id.midname).Text = person.Middlename;
            FindViewById<EditText>(Resource.Id.natid).Text = person.NatId;

            var cmbgender = FindViewById<Spinner>(Resource.Id.cmbgender);
            var cmbmarital = FindViewById<Spinner>(Resource.Id.cmbmarital);
            cmbgender.SetSelection(GetSpinnerIndex(cmbgender, person.Gender));
            cmbmarital.SetSelection(GetSpinnerIndex(cmbmarital, person.Marital));
        }

        private static int GetSpinnerIndex(Spinner spinner, string val)
        {
            var index = 0;
            for (var i = 0; i < spinner.Count; i++)
            {
                var x = spinner.GetItemAtPosition(i).ToString();
                var y = val;
                if (spinner.GetItemAtPosition(i).ToString().Equals(val))
                {
                    index = i;
                }
            }
            return index;
        }

        public Bitmap LoadAndResizeBitmapCustom(string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            var options = new BitmapFactory.Options { InJustDecodeBounds = true, InPreferredConfig = Bitmap.Config.Rgb565 };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            var outHeight = options.OutHeight;
            var outWidth = options.OutWidth;
            var inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                    ? outHeight / height
                        : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            var resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            // Images are being saved in landscape, so rotate them back to portrait if they were taken in portrait
            var mtx = new Matrix();
            var exif = new ExifInterface(fileName);
            var orientation = exif.GetAttribute(ExifInterface.TagOrientation);

            switch (orientation)
            {
                case "6": // portrait
                    mtx.PreRotate(0);
                    resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, mtx, false);
                    mtx.Dispose();
                    mtx = null;
                    break;
                case "1": // landscape
                    break;
                default:
                    mtx.PreRotate(0);
                    resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, mtx, false);
                    mtx.Dispose();
                    mtx = null;
                    break;
            }



            return resizedBitmap;
        }

        #endregion

        #region Events

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            switch (requestCode)
            {
                case 1: // CAMERA
                    if (resultCode == Result.Ok)
                    {
                        //DataCenter.setFace(App.bitmap, view);

                        // Crop image
                        var intent = new Intent(this, typeof(CropImage.CropImage));
                        intent.PutExtra("image-path", App._file.Path);
                        _currPerson.Photo = App._file.Path;
                        intent.PutExtra("scale", true);
                        StartActivityForResult(intent, 4);
                    }
                    break;

                case 2: // FINGERPRINT

                    if (resultCode == Result.Ok)
                    {
                        var view = FindViewById<ImageView>(Resource.Id.fingeprintpic);
                        view.SetImageBitmap(ScanningState.ScannedBitmap);

                        // Save bitmap
                        var path = App.Findir + _currPerson.Surname + "_" + _currPerson.PersonId;
                        using (var os = new FileStream(path, FileMode.Create))
                        {
                            var fbitmap = ScanningState.ScannedBitmap;
                            fbitmap.Compress(Bitmap.CompressFormat.Png, 95, os);
                        }

                        _currPerson.Fingerprint = path;
						view.SetScaleType (ImageView.ScaleType.CenterCrop);
                        // take wsq from the ScanningState.scannedWsqFpImage array
                        //Android.Util.Log.Info("FINGERPRINT", String.Format("wsq size: %d", ScanningState.ScannedWsqFpImage.Count));
                    }
                    break;

                case 3: // SIGNATURE
                    if (resultCode == Result.Ok)
                    {
                        var view = FindViewById<ImageView>(Resource.Id.sigpic);
                        App.Sigbitmap = LoadAndResizeBitmapCustom(App.Sigfile.Path, view.Width, view.Height);
                        view.SetImageBitmap(App.Sigbitmap);
                        _currPerson.Signature = App.Sigfile.Path;
                    }
                    break;

                case 4: // CROP
                    if (resultCode == Result.Ok)
                    {
                        var view = FindViewById<ImageView>(Resource.Id.photopic);
                        App.Bitmap = LoadAndResizeBitmapCustom(App._file.Path, view.Width, view.Height);
                        _currPerson.Photo = App._file.Path;
                        view.SetImageBitmap(App.Bitmap);
						view.SetScaleType (ImageView.ScaleType.CenterCrop);
                    }
                    break;
            }


        }

        private void spinner_GenderItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var adapterView = sender as AdapterView;
            ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
        }

        private void spinner_MaritalItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var adapterView = sender as AdapterView;
            ((TextView) adapterView?.GetChildAt(0))?.SetTextColor(Color.ParseColor("#333333"));
        }

        public void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            _dateHandle = e.Date;
            _editHandle.Text = _dateHandle.ToString("dd/MM/yyyy");
        }

        protected override Dialog OnCreateDialog(int id)
        {
            switch (id)
            {
                case DateDialogId:
                    return new DatePickerDialog(this, OnDateSet, _dateHandle.Year, _dateHandle.Month - 1, _dateHandle.Day);
            }
            return null;
        }

        #endregion
	}

    public static class App
    {
        public static File _file;
        public static File Dir;
        public static Bitmap Bitmap;
        public static File Sigfile;
        public static File Sigdir;
        public static Bitmap Sigbitmap;
        public static File Finfile;
        public static File Findir;
        public static Bitmap Finbitmap;
    }
}

