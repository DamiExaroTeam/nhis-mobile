﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace NHISmobile
{
    [Activity (Label = "VCsavedReg")]			
	public class VCsavedReg : Activity
	{
		private Person _selectedItem;

		#region Constructor

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

			SetContentView (Resource.Layout.VCSavedReg);

			// Get incomplete registrations from database
			var regs = DataCenter.GetSavedVcRegistrations ();

			// Populate the list
			var lv = (ListView) FindViewById(Resource.Id.vcsavedlv);
			var contactsAdapter = new ContactsAdapter (this, regs);
			lv.Adapter = contactsAdapter;

			// Create event listener for click
			lv.ItemClick += listView_ItemClick;

			// Create event listner for buttons
			var btn = FindViewById<Button> (Resource.Id.editregbtn);
			btn.Click += delegate {
				// Pass data to activity and start
				var activity = new Intent (this, typeof(VCregistration));
				activity.PutExtra ("SavedReg", _selectedItem.XmlSerializeToString());
				StartActivity(activity);
                Finish();
			};

			// Delete listener
			btn = FindViewById<Button> (Resource.Id.delregbtn);
			btn.Click += delegate {
				DataCenter.DeleteSavedProfile(_selectedItem);
				regs.Remove(_selectedItem);
				contactsAdapter.NotifyDataSetChanged();

				// disable buttons
				ToggleButtons(false);
			};

			// New registration listener
			FindViewById<Button> (Resource.Id.createNewReg).Click += delegate {
				StartActivity(typeof(VCregistration));
				Finish();
			};
		}

		#endregion

		#region Events

		public void listView_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
		    var listView = sender as ListView;
		    var contactsAdapter = listView?.Adapter as ContactsAdapter;
		    if (contactsAdapter != null)
		        _selectedItem = contactsAdapter.GetPersonAtPosition(e.Position);
		    ToggleButtons (true);
		}

		#endregion

		public void ToggleButtons (bool val)
		{
			FindViewById<Button> (Resource.Id.editregbtn).Clickable = val;
			FindViewById<Button> (Resource.Id.delregbtn).Clickable = val;
			FindViewById<Button> (Resource.Id.editregbtn).Alpha = val ? 1 : (float)0.25;
			FindViewById<Button> (Resource.Id.delregbtn).Alpha = val ? 1 : (float)0.25;
		}
	}
}

