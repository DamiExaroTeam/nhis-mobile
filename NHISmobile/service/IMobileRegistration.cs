﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using VCmobileEntities;

namespace NHISmobile
{
    [ServiceContract]
    public interface IMobileRegistration
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "GetVoluntaryEntity/{entityId}")]
        Entity GetVoluntaryEntity(string entityId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "LoginMobileUser/{username}/{password}/{macAddress}")]
        long LoginMobileUser(string username, string password, string macAddress);

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "SaveVoluntaryBiometricInfo")]        
        //bool SaveVoluntaryBiometricInfo(EntityImages images);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "GetLgaList/{stateName}")]
        List<Lga> GetLgaList(string stateName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "GetStateList")]
        List<State> GetStateList();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "GetHCPSearchedList/{lgaId}/{hcpName}")]
        List<Hcf> GetHcpSearchedList(string lgaId, string hcpName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "GetHmoForVoucher/{voucherNumber}")]
        string GetHmoForVoucher(string voucherNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "CreateTempVoluntaryContributor")]
        RegistrationResult CreateTempVoluntaryContributor(RegistrationResult registrationModel);
    }
}
