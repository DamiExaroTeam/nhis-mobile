using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

namespace NHISmobile
{
    public static class MobileService
    {
        //static string service = "http://10.10.10.251:47191/Service.svc/";
        static string _service = "http://41.217.204.230:565/Service.svc/";
        //static string service = "http://10.10.10.30:565/service.svc/";
        static string _servicepw = "NHISmobileApp";
        public static string Error = string.Empty;
        
        private static async Task<bool> CheckServerTimeUpdated()
        {
            try
            {
                if (Helper.TimeDifference == -1)
                {
                    await GetServerTime();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static string CreateToken()
        {
            var token = Hashing.Hash(_servicepw, Encoding.UTF8.GetBytes(Helper.GetDateTimeNow()));
            return Convert.ToBase64String(token);
        }

        public static int LoginOperator(string logindata)
        {
            if (!CheckServerTimeUpdated().Result)
                return -1; 
            
            var val = 0;

            var data = new Response { Data = logindata, Token = CreateToken() };
            var result = PostObjectData<Response>("LoginOperator", data);

            if (string.IsNullOrEmpty(result.Error))
            {
                if (result.response == "1")
                {
                    if (Helper.Operator == null)
                        Helper.Operator = new Operator();

                    Helper.Operator.Firstname = result.Firstname;
                    Helper.Operator.Surname = result.Surname;
                    return 1;
                }
                if (result.response == "2")
                    return 2;
                if (result.response == "3")
                    return 3;
            }
            else
            {
                // Log error somewhere
                return -1;
            }

            return val;
        }

        public static Voucher SearchForVoucher(string vouchernum)
        {
            if (!CheckServerTimeUpdated().Result)
                return null;

            Error = string.Empty;
            var voucher = new Voucher
            {
                VoucherId = long.Parse(vouchernum),
                Security = StringCipher.Encrypt(Helper.Operator.Username + "|" + Helper.Operator.Password, Helper.GetDateTimeNow())
            };

			var data = new Response { Data = voucher.ToJson(), Token = CreateToken() };
            var result = PostObjectData<Response>("SearchForVoucher", data);

            if (string.IsNullOrEmpty(result.Error))
            {
                if (result.response == "1")
                {
                    return result.Voucher;
                }
                Error = result.response;
                return null;
            }
            // Log error somewhere
            return null;
        }

        public static List<Person> SearchForMember(Person person)
        {
            if (!CheckServerTimeUpdated().Result)
                return null;

            Error = string.Empty;
            var members = new List<Person>();

			var data = new Response { Data = person.ToJson(), Token = CreateToken() };
            var result = PostObjectData<Response>("SearchForMember", data);

            // if there was an error

            if (string.IsNullOrEmpty(result.Error))
            {
                if (result.response == "1")
                    return result.Searchresult;
                Error = result.response;
                return null;
            }
            // Log error somewhere
            return null;
        }

        public static Person GetMemberImages(Person person)
        {
            if (!CheckServerTimeUpdated().Result)
                return null;

            Error = string.Empty;
            person.Security = StringCipher.Encrypt(Helper.Operator.Username + "|" + Helper.Operator.Password, Helper.GetDateTimeNow());
			var data = new Response { Data = person.ToJson(), Token = CreateToken() };
            var result = PostObjectData<Response>("GetMemberImages", data);

            // if there was an error
            if (string.IsNullOrEmpty(result.Error))
            {
				if (result.response == "1") {
					// decompress images
					if (!string.IsNullOrEmpty(result.Searchperson.PhotoByteZipped)) {
						result.Searchperson.PhotoByte = Unzip(result.Searchperson.PhotoByteZipped);
					}

					if (!string.IsNullOrEmpty(result.Searchperson.SignatureByteZipped)) {
						result.Searchperson.SignatureByte = Unzip(result.Searchperson.SignatureByteZipped);
					}

					if (!string.IsNullOrEmpty(result.Searchperson.FingerprintByteZipped)) {
						result.Searchperson.FingerprintByte = Unzip(result.Searchperson.FingerprintByteZipped);
					}

					return result.Searchperson;
				}
                Error = result.response;
                return null;
            }
            // Log error somewhere
            return null;
        }

        public static Response PostObjectData<T>(string method, Response obj)
        {
            var result = new Response();

            try
            {
				var postData = obj.ToJson();

                //convert the data as a byte array
                var byteArray = Encoding.UTF8.GetBytes(postData);

                //create an HTTP request to the URL that we need to invoke
                var request = (HttpWebRequest)WebRequest.Create(_service + method);
                request.ContentLength = byteArray.Length;
                request.Timeout = 40000;
                request.ContentType = "application/json; charset=utf-8"; // set the content type to JSON
                request.Method = "POST"; //make an HTTP POST

                using (var dataStream = request.GetRequestStream())
                {
                    //initiate the request
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                // Get the response.
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        var reader = new StreamReader(stream, Encoding.UTF8);
                        var responseString = reader.ReadToEnd();
                        if (!string.IsNullOrEmpty(responseString))
                        {
							result = responseString.FromJson<Response>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
            }

            return result;
        }

        public static async Task<string> GetServerTime()
        {
            var servertime = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(_service + "TestConnection");
                request.Timeout = 20000;
                request.ContentType = "application/json; charset=utf-8"; // set the content type to JSON
                request.Method = "Get"; //make an HTTP POST

                // Get the response.
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        var reader = new StreamReader(stream, Encoding.UTF8);
                        var responseString = reader.ReadToEnd();
                        if (!string.IsNullOrEmpty(responseString))
                        {
							servertime = responseString.FromJson<string>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
				return ex.Message;
            }

            return servertime;
        }

		public static byte[] Unzip(string compressedText) {
			var bytes = Convert.FromBase64String(compressedText);
			using (var msi = new MemoryStream(bytes))
			using (var mso = new MemoryStream()) {
				using (var gs = new GZipStream(msi, CompressionMode.Decompress)) {
					//gs.CopyTo(mso);
					CopyTo(gs, mso);
				}

				return mso.ToArray();
			}
		}

		public static void CopyTo(Stream src, Stream dest)
		{
			var bytes = new byte[4096];

			int cnt;

			while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
			{
				dest.Write(bytes, 0, cnt);
			}
		}
    }
}