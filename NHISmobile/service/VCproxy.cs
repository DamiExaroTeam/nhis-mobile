using System;
using System.Net;
using System.ServiceModel;

namespace NHISmobile
{
    public class VcProxy
    {
        ChannelFactory<IMobileRegistration> _cf;

        public IMobileRegistration Channel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public VcProxy()
        {
            //Get from config
            ServiceBaseAddress = "http://exaro.dyndns-office.com:1801/MobileRegistrationService.svc";

            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 4194304;//4 mb
            binding.Security.Mode = BasicHttpSecurityMode.None;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.SendTimeout = TimeSpan.FromMinutes(60);
            binding.CloseTimeout = TimeSpan.FromMinutes(60);
            binding.OpenTimeout = TimeSpan.FromMinutes(60);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(60);
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReaderQuotas.MaxArrayLength = 4194304;
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };

            _cf = new ChannelFactory<IMobileRegistration>(binding, ServiceBaseAddress);
            Channel = _cf.CreateChannel();
        }
    }
}