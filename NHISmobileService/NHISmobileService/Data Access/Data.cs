﻿using NHISmobileService.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace NHISmobileService.Data_Access
{
    public static class Data
    {
        public static List<Person> SearchForMember(Person person)
        {
            var result = new List<Person>();

            var query = @"select e.entity_id, e.firstname, e.surname, e.middlename, e.mar_status, e.dob, e.gender, e.national_id, e.email_address, e.gsm_number, 
                            lga.lga_id as lga_id, lga.name as lga, st.state_id as state_id, st.name as state,
                            h.hmo_nm, v.cover_from, v.cover_to, hcp.hcp_nm
                            from nhis_entity.entity e left join nhis_entity.spatial s on e.spatial_id = s.spatial_id
                            left join nhis_entity.state st on st.state_id = s.state_id
                            left join nhis_entity.lga lga on lga.lga_id = s.lga_id
                            left join nhis_entity.entity_voluntary v on v.entity_id = e.entity_id
                            left join nhis_entity.hmo_obs h on h.hmo_id = v.hmo_id
                            left join nhis_entity.hcp_obs hcp on hcp.hcp_id = v.hcp_id
                            where e.del_flg = 0";


            if (person.PersonId != 0)
                query += " and e.entity_id = " + person.PersonId;

            if (!string.IsNullOrEmpty(person.Firstname))
                query += " and lower(e.firstname) like '%" + person.Firstname + "%'";

            if (!string.IsNullOrEmpty(person.Surname))
                query += " and lower(e.surname) like '%" + person.Surname + "%'";

            if (!string.IsNullOrEmpty(person.Email))
                query += " and lower(e.email_address) like '%" + person.Email + "%'";

            if (!string.IsNullOrEmpty(person.Dob))
                query += " and lower(e.dob) like '%" + person.Dob + "%'";

            if (!string.IsNullOrEmpty(person.Phonenum))
                query += " and lower(e.gsm_number) like '%" + person.Phonenum + "%'";

            using (var dset = new DataSet())
            {
                OracleDataProv.RunProcedure(query, null, dset);
                var searchresults = new Dictionary<long, Person>();

                for (var i = 0; i < dset.Tables[0].Rows.Count; i++)
                {
                    var row = dset.Tables[0].Rows[i];

                    var personid = long.Parse(row["entity_id"].ToString());

                    if (searchresults.ContainsKey(personid))
                    {
                        if (DateTime.Parse(searchresults[personid].CoverTo) > DateTime.Parse(row["cover_to"].ToString()))
                        {
                        }
                        else
                        {
                            searchresults[personid].CoverTo = DateTime.Parse(row["cover_to"].ToString()).ToString("dd/MM/yyyy");
                            searchresults[personid].CoverFrom = DateTime.Parse(row["cover_from"].ToString()).ToString("dd/MM/yyyy");
                        }
                    }
                    else
                    {
                        var newp = new Person();
                        newp.PersonId = personid;
                        newp.Firstname = row["firstname"].ToString();
                        newp.Surname = row["surname"].ToString();
                        newp.Middlename = row["middlename"].ToString();
                        newp.Marital = row["mar_status"].ToString();
                        newp.Gender = row["gender"].ToString();
                        newp.NatId = row["national_id"].ToString();
                        newp.Email = row["email_address"].ToString();
                        newp.Phonenum = row["gsm_number"].ToString();

                        newp.Hospital = row["hcp_nm"].ToString();
                        newp.Hmo = row["hmo_nm"].ToString();
                        if (!string.IsNullOrEmpty(row["cover_from"].ToString()))
                            newp.CoverFrom = DateTime.Parse(row["cover_from"].ToString()).ToString("dd/MM/yyyy");
                        if (!string.IsNullOrEmpty(row["cover_to"].ToString()))
                            newp.CoverTo = DateTime.Parse(row["cover_to"].ToString()).ToString("dd/MM/yyyy");

                        // Set date format..yeah a bit unecessary but whareva
                        if (!string.IsNullOrEmpty(row["dob"].ToString()))
                            newp.Dob = DateTime.Parse(row["dob"].ToString()).ToString("dd/MM/yyyy");

                        if (!string.IsNullOrEmpty(row["state_id"].ToString()))
                        {
                            newp.State = new State()
                            {
                                StateName = row["state"].ToString(),
                                StateId = int.Parse(row["state_id"].ToString())
                            };
                        }

                        if (!string.IsNullOrEmpty(row["lga_id"].ToString()))
                        {
                            newp.Lga = new Lga()
                            {
                                LgAname = row["lga"].ToString(),
                                LgAid = int.Parse(row["lga_id"].ToString())
                            };
                        }

                        result.Add(newp);
                        searchresults.Add(newp.PersonId, newp);
                    }
                }
            }

            return result;
        }

        public static Voucher SearchForVoucher(long vouchernum)
        {
            // Voucher Status
            // 1) Created
            // 2) Redeemed
            // 3) Cancelled

            var query = @"select e.entity_id, e.firstname, e.surname, e.middlename, e.mar_status, e.dob, e.gender, e.national_id, e.email_address, e.gsm_number
                            , t.date_redeemed, t.status, h.hmo_id, h.hmo_nm, v.cover_from, v.cover_to, hcp.hcp_nm
                            from nhis_entity.membership_voucher t
                            left join nhis_entity.entity e on e.entity_id = t.entity_id_redeemed
                            left join nhis_entity.membership_voucher_batch b on b.batch_number = t.membership_voucher_batch_id
                            left join nhis_entity.hmo_obs h on h.hmo_id = b.hmo_id
                            left join nhis_entity.entity_voluntary v on v.entity_id = e.entity_id
                            left join nhis_entity.hcp_obs hcp on hcp.hcp_id = v.hcp_id
                            left join nhis_entity.entity_voluntary v on v.entity_id = e.entity_id
                            where t.membership_voucher_number = :vouchernumber";

            OracleParameter[] param =
            {   
                new OracleParameter("vouchernumber",  vouchernum)
            };

            using (var dset = new DataSet())
            {
                OracleDataProv.RunProcedure(query, param, dset);

                var voucher = new Voucher();

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    voucher.VoucherId = vouchernum;
                    voucher.Status = row["status"].ToString() == "1" ? "Activated" : row["status"].ToString() == "2" ? "Redeemed" : "Cancelled";
                    voucher.Hmo = row["hmo_nm"].ToString();

                    if (!string.IsNullOrEmpty(row["entity_id"].ToString()))
                    {
                        voucher.Redeemer = new Person
                        {
                            PersonId = int.Parse(row["entity_id"].ToString()),
                            Firstname = row["firstname"].ToString(),
                            Surname = row["surname"].ToString(),
                            Middlename = row["surname"].ToString(),
                            Marital = row["mar_status"].ToString(),
                            Gender = row["gender"].ToString(),
                            NatId = row["national_id"].ToString(),
                            Phonenum = row["gsm_number"].ToString(),
                            Email = row["email_address"].ToString(),
                            Hospital = row["hcp_nm"].ToString(),
                            Hmo = row["hmo_nm"].ToString()
                        };

                        if (!string.IsNullOrEmpty(row["cover_from"].ToString()))
                            voucher.Redeemer.CoverFrom = DateTime.Parse(row["cover_from"].ToString()).ToString("dd/MM/yyyy");
                        if (!string.IsNullOrEmpty(row["cover_to"].ToString()))
                            voucher.Redeemer.CoverTo = DateTime.Parse(row["cover_to"].ToString()).ToString("dd/MM/yyyy");

                        // Set date format..yeah a bit unecessary but whareva
                        if (!string.IsNullOrEmpty(row["dob"].ToString()))
                        {
                            voucher.Redeemer.Dob = DateTime.Parse(row["dob"].ToString()).ToString("dd/MM/yyyy");
                        }

                        if (!string.IsNullOrEmpty(row["date_redeemed"].ToString()))
                        {
                            voucher.DateRedeemed = DateTime.Parse(row["date_redeemed"].ToString()).ToString("dd/MM/yyyy");
                        }
                    }
                }

                return voucher;
            }
        }

        public static Person GetMemberImages(long personid)
        {
            var query = @"select i.image, i.image_type_id
                            from nhis_entity.entity e
                            left join 
                            (select entity_id, image_id as image_id from nhis_entity.entity_image where entity_id = :entity_id and rownum < 4 order by image_id desc) ei
                            on ei.entity_id = e.entity_id
                            left join nhis_entity.image i on i.image_id = ei.image_id
                            where e.entity_id = :entity_id";

            OracleParameter[] param =
            {   
                new OracleParameter("entity_id",  personid)
            };

            using (var dset = new DataSet())
            {
                try
                {
                    OracleDataProv.RunProcedure(query, param, dset);
                }
                catch(Exception ex)
                {
                    return new Person() { Firstname = ex.Message };
                }

                var person = new Person();
                person.PersonId = personid;

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty(row["image_type_id"].ToString()))
                    {
                        switch (row["image_type_id"].ToString())
                        {
                            case "1": person.PhotoByteZipped = Zip(GetResizedImage((byte[])row["image"])); break;
                            case "2": person.FingerprintByteZipped = Zip(GetResizedImage((byte[])row["image"])); break;
                            case "3": person.SignatureByteZipped = Zip(GetResizedImage((byte[])row["image"])); break;
                        }
                    }
                }

                return person;
            }
        }

        public static string SaveMemberImages(Person person)
        {
            var rowsChanged = 0;

            try
            {
                // Save Picture
                rowsChanged = rowsChanged + SaveImage(person.PersonId, Unzip(person.PhotoByteZipped), 1);

                // Save Fingerprint
                rowsChanged = rowsChanged + SaveImage(person.PersonId, Unzip(person.FingerprintByteZipped), 2);

                // Save Signature
                rowsChanged = rowsChanged + SaveImage(person.PersonId, Unzip(person.SignatureByteZipped), 3);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            // Just leaving this in case I decide to make checks using the row count...for some reason.
            return rowsChanged.ToString();
        }

        private static int SaveImage(long nhisId, byte[] image, int type)
        {
            var rowsChanged = 0;
            var imgSeq = GetNextSequenceId("IMAGE_ID");

            var query = "insert into nhis_entity.image (image_id, image, image_type_id) values (:image_seq, :image, :type)";
            OracleParameter[] imgParams =
            {                 
                new OracleParameter("image", image),
                new OracleParameter("image_seq", imgSeq),
                new OracleParameter("type", type)
            };
            
            rowsChanged = rowsChanged + OracleDataProv.ExecuteQuery2(query, imgParams);

            var otherQuery = "insert into nhis_entity.entity_image (entity_id, image_id) values (:entity_id, :image_seq)";

            OracleParameter[] linkParam =
                    {                 
                        new OracleParameter("entity_id", nhisId),
                        new OracleParameter("image_seq", imgSeq),
                    };

            rowsChanged = rowsChanged + OracleDataProv.ExecuteQuery2(otherQuery, linkParam);

            // If no issues with previous query, Add details into Entity-Image table
            // ********
//            query = @"select ei.entity_id, ei.image_id, i.image_type_id, i.image
//                    from nhis_entity.entity e
//                    left join 
//                    (select entity_id, image_id as image_id from nhis_entity.entity_image where entity_id = :entity_id and rownum < 4 order by image_id desc) ei
//                    on ei.entity_id = e.entity_id
//                    left join nhis_entity.image i on i.image_id = ei.image_id
//                    where e.entity_id = :entity_id";

//            OracleParameter[] param =
//            {   
//                new OracleParameter("entity_id",  nhisID)
//            };

//            using (DataSet dset = new DataSet())
//            {
//                OracleDataProv.RunProcedure(query, param, dset);

//                if (dset.Tables[0].Rows.Count != 0 && !string.IsNullOrEmpty(dset.Tables[0].Rows[0]["image_id"].ToString()))
//                {
//                    foreach (DataRow row in dset.Tables[0].Rows)
//                    {
//                        int imgtype = int.Parse(row["image_type_id"].ToString());
//                        int imgid = int.Parse(row["image_id"].ToString());

//                        if (imgtype == type)
//                        {
//                            string otherQuery = "update nhis_entity.entity_image set image_id = :image_id2 where image_id = :image_id";

//                            OracleParameter[] linkParam =
//                            {                 
//                                new OracleParameter("image_id2", img_seq),
//                                new OracleParameter("image_id", imgid),
//                            };

//                            rowsChanged = rowsChanged + OracleDataProv.ExecuteQuery2(otherQuery, linkParam);
//                        }
//                    }
//                }
//                else
//                {
//                    string otherQuery = "insert into entity_image (entity_id, image_id) values (:entity_id, :image_seq)";

//                    OracleParameter[] linkParam =
//                    {                 
//                        new OracleParameter("entity_id", nhisID),
//                        new OracleParameter("image_seq", img_seq),
//                    };

//                    rowsChanged = rowsChanged + OracleDataProv.ExecuteQuery2(otherQuery, linkParam);
//                }
//            }

            return rowsChanged;
        }

        public static long GetNextSequenceId(string sequence)
        {
            var query = "SELECT " + sequence + ".NEXTVAL FROM DUAL";

            var dset = new DataSet();
            OracleDataProv.RunProcedure2(query, dset);

            if (dset.Tables[0].Rows.Count != 0)
                return long.Parse(dset.Tables[0].Rows[0][0].ToString());

            return 0;
        }

        #region All of this in an effort to reduce size of image to the MAX!

        private static byte[] GetResizedImage(byte[] img)
        {
            using (var stream = new MemoryStream(img))
            {
                var image = Image.FromStream(stream);
                Image newimage = ResizeImage(image);
                return ImageToByteArray(newimage);
            }
        }

        public static byte[] ImageToByteArray(Image imageIn)
        {
            var parameters = new EncoderParameters(1);
            var codecInfo = GetEncoderInfo(ImageFormat.Jpeg);
            using (var ms = new MemoryStream())
            {
                parameters.Param[0] = new EncoderParameter(
                Encoder.Quality, 75L);
                imageIn.Save(ms, codecInfo, parameters);
                return ms.ToArray();
            }
        }

        public static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageEncoders().ToList().Find(codec => codec.FormatID == format.Guid);
        }

        private static Bitmap ResizeImage(Image image)
        {
            var maxWidth = 250;
            var maxHeight = 400;
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        public static string Zip(byte[] img)
        {
            using (var msi = new MemoryStream(img))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return Convert.ToBase64String(mso.ToArray());
            }
        }

        public static byte[] Unzip(string compressedText)
        {
            var bytes = Convert.FromBase64String(compressedText);
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return mso.ToArray();
            }
        }


        public static void CopyTo(Stream src, Stream dest)
        {
            var bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        } 

        #endregion
    }
}
