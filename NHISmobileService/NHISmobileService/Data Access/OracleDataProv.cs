﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;

namespace NHISmobileService.Data_Access
{
    public static class OracleDataProv
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["OracleConnectionString"].ToString();
        private static readonly string ConnectionString2 = ConfigurationManager.ConnectionStrings["OracleConnectionString2"].ToString();
        public static OracleConnection Connection;
        public static OracleConnection Connection2;
        public static OracleCommand Command;

        public static void CreateConnection()
        {
            if (Connection == null)
            {
                try
                {
                    Connection = new OracleConnection(ConnectionString);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static void CreateConnection2()
        {
            if (Connection2 == null)
            {
                Connection2 = new OracleConnection(ConnectionString2);
            }
        }

        public static void CreateTransactionCommand()
        {
            if (Command?.Transaction?.Connection != null && Command.Transaction.Connection.State == ConnectionState.Open)
                Command.Transaction.Connection.Close();

            if (Connection == null)
                CreateConnection();

            if (Connection.State == ConnectionState.Closed)
                Connection.Open();

            var transaction = Connection.BeginTransaction();

            Command = Connection.CreateCommand();
            Command.Transaction = transaction;
        }

        public static int ExecuteQuery(OracleParameter[] parameters)
        {
            var cmd = Command;

            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            if (parameters != null && parameters.Length > 0)
            {
                cmd.Parameters.Clear();

                foreach (OracleParameter p in parameters)
                {
                    if (p != null)
                        cmd.Parameters.Add(p);
                }
            }

            var iRowsAffected = cmd.ExecuteNonQuery();

            return iRowsAffected;
        }

        public static int ExecuteQuery(string strQuery, OracleParameter[] parameters)
        {
            int iRowsAffected = 0;

            try
            {
                if (Connection == null)
                {
                    CreateConnection();
                }

                if (Connection != null && Connection.State != ConnectionState.Open)
                    Connection.Open();

                if (Connection != null)
                    using (var cmd = Connection.CreateCommand())
                    {
                        cmd.CommandText = strQuery;
                        if (parameters != null && parameters.Length > 0)
                        {
                            foreach (var p in parameters)
                            {
                                if (p.Value == null)
                                    p.Value = DBNull.Value;

                                cmd.Parameters.Add(p);
                            }
                        }

                        cmd.Connection = Connection;
                        iRowsAffected = cmd.ExecuteNonQuery();
                    }
            }
            finally
            {
                Connection?.Close();
            }

            return iRowsAffected;
        }

        public static int ExecuteQuery2(string strQuery, OracleParameter[] parameters)
        {
            int iRowsAffected;

            try
            {
                if (Connection2 == null)
                {
                    CreateConnection();
                }

                if (Connection2 != null && Connection2.State != ConnectionState.Open)
                    Connection2.Open();

                using (var cmd = Connection2.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        foreach (var p in parameters)
                        {
                            if (p.Value == null)
                                p.Value = DBNull.Value;

                            cmd.Parameters.Add(p);
                        }
                    }

                    cmd.Connection = Connection2;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                Connection2?.Close();
            }

            return iRowsAffected;
        }

        public static void RunProcedure(string strQuery, OracleParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (Connection == null)
                {
                    CreateConnection();
                }

                if (Connection.State != ConnectionState.Open)
                    Connection.Open();

                using (var cmd = Connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (var i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    OracleDataAdapter oracleDa = new OracleDataAdapter();
                    oracleDa.SelectCommand = cmd;
                    oracleDa.Fill(dataSet);
                    Connection.Close();
                }
            }
            finally
            {
                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                }
            }
        }

        public static void RunProcedure2(string strQuery, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (Connection2 == null)
                {
                    CreateConnection2();
                }

                if (Connection2.State != ConnectionState.Open)
                    Connection2.Open();

                using (var cmd = Connection2.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    OracleDataAdapter oracleDa = new OracleDataAdapter
                    {
                        SelectCommand = cmd
                    };
                    oracleDa.Fill(dataSet);
                    Connection2.Close();
                }
            }
            finally
            {
                if (Connection2.State == ConnectionState.Open)
                {
                    Connection2.Close();
                }
            }
        }

        public static int GetIdNumber(OracleCommand cmd)
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            cmd.CommandText = "select last_insert_id();";
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public static void CloseQuery()
        {
            Connection.Close();
        }
    }
}
