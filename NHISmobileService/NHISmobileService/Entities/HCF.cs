using System.Runtime.Serialization;

namespace NHISmobileService.Entities
{
    [DataContract]
    public class Hcf
    {
        [DataMember]
        public long HcfId { get; set; }

        [DataMember]
        public string HcfName { get; set; }

        public long GetHcfId() { return HcfId; }

        public string GetHcfName() { return HcfName; }
    }
}