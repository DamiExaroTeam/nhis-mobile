﻿using System.Collections.Generic;

namespace NHISmobileService.Entities
{
	public class Lga
	{
        public int LgAid { get; set; }

		public string LgAname { get; set; }

		public List<string> Hospitals {get; set; }

		public string SelectedHospital { get; set; }

		public override string ToString ()
		{
			return LgAname;
		}
	}
}

