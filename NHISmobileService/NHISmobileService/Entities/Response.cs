﻿using System.Collections.Generic;

namespace NHISmobileService.Entities
{
    public class Response
    {
        public string Data { get; set; }

        public string Token { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public string response { get; set; }

        public string Error { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public Person Searchperson { get; set; }

        public List<Person> Searchresult { get; set; }

        public Voucher Voucher { get; set; }
    }
}