﻿using System.Collections.Generic;

namespace NHISmobileService.Entities
{
	public class State
	{
        public int StateId { get; set; }

        public string StateName { get; set; }

		public List<Lga> LgAs {get; set; }

		public Lga SelectedLga { get; set; }

		public override string ToString ()
		{
			return StateName;
		}
	}
}

