namespace NHISmobileService.Entities
{
    public class Voucher
    {
        public long VoucherId { get; set; }

        public Person Redeemer { get; set; }

        public string Security { get; set; }

        public string Status { get; set; }

        public string Hmo { get; set; }

        public string DateRedeemed { get; set; }
    }
}