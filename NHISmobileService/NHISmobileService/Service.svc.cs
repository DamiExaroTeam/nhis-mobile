﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;
using NHISmobileService.Contract;
using NHISmobileService.Data_Access;
using NHISmobileService.Entities;
using NHISmobileService.Helpers;

namespace NHISmobileService
{
    public class Service : IMobileRegistration
    {
        public string TestConnection()
        {
            return DateTime.Now.ToString();
        }

        public Response LoginOperator(string data, string token)
        {
            var response = new Response();
            var valres = Hashing.ValidateHash(Convert.FromBase64String(token));

            if (valres != 0)
            {
                var val = valres == 1 ? StringCipher.Decrypt(data, DateTime.Now.ToString("ddMMyyyyhhmm")) : StringCipher.Decrypt(data, DateTime.Now.AddMinutes(1).ToString("ddMMyyyyhhmm"));
                var username = val.Split('|')[0].ToString();
                var password = val.Split('|')[1].ToString();

                if (username == "admin" && password == "password")
                {
                    response.response = "1";
                }
                else
                    response.response = "3";
            }
            else
                response.response = "2";

            return response;
        }

        public Response SearchForMember(string data, string token)
        {
            var response = new Response();
            try
            {
                var valres = Hashing.ValidateHash(Convert.FromBase64String(token));

                if (valres != 0 || !string.IsNullOrEmpty(data))
                {
                    var person = data.FromJson<Person>();

                    if (string.IsNullOrEmpty(person.Security))
                        response.Error = "Security details failed/missing";
                    else
                    {
                        if (person != null && VerifyLoginDetails(person.Security, valres))
                        {
                            // Search database for person
                            response.Searchresult = Data.SearchForMember(person);
                            response.response = "1";
                        }
                        else
                            response.response = "3";
                    }
                }
                else
                    response.response = "2";
            }
            catch(Exception ex)
            {
                response.Error = ex.Message;
            }

            return response;
        }

        public Response SearchForVoucher(string data, string token)
        {
            var response = new Response();
            var valres = Hashing.ValidateHash(Convert.FromBase64String(token));

            if (valres != 0 || !string.IsNullOrEmpty(data))
            {
                var voucher = data.FromJson<Voucher>();

                if (VerifyLoginDetails(voucher.Security, valres))
                {
                    // Search database for person
                    response.Voucher = Data.SearchForVoucher(voucher.VoucherId);
                    response.response = "1";
                }
                else
                    response.response = "3";
            }
            else
                response.response = "2";

            return response;
        }

        public Response GetMemberImages(string data, string token)
        {         
            var response = new Response();

            try
            {
                var valres = Hashing.ValidateHash(Convert.FromBase64String(token));

                if (valres != 0 || !string.IsNullOrEmpty(data))
                {
                    var person = data.FromJson<Person>();

                    if (VerifyLoginDetails(person.Security, valres))
                    {
                        // Search database for person
                        response.Searchperson = Data.GetMemberImages(person.PersonId);
                        response.response = "1";
                    }
                    else
                        response.response = "3";
                }
                else
                    response.response = "2";
            }
            catch (Exception ex)
            {
                response.Error = ex.Message;
            }

            return response;
        }

        public Response SaveMemberImages(string data, string token)
        {
            var response = new Response();

            try
            {
                var valres = Hashing.ValidateHash(Convert.FromBase64String(token));

                if (valres != 0 || !string.IsNullOrEmpty(data))
                {
                    var person = data.FromJson<Person>();

                    if (VerifyLoginDetails(person.Security, valres))
                    {
                        // Search database for person
                        var result = Data.SaveMemberImages(person);

                        if (result == "6")
                            response.response = "1";
                        else
                            response.Error = result.ToString();
                    }
                    else
                        response.response = "3";
                }
                else
                    response.response = "2";
            }
            catch (Exception ex)
            {
                response.Error = ex.Message;
            }

            return response;
        }

        private bool VerifyLoginDetails(string security, int valres)
        {
            // Test environment
            var val = valres == 1 ? StringCipher.Decrypt(security, DateTime.Now.ToString("ddMMyyyyhhmm")) : StringCipher.Decrypt(security, DateTime.Now.AddMinutes(1).ToString("ddMMyyyyhhmm"));
            var username = val.Split('|')[0].ToString();
            var password = val.Split('|')[1].ToString();
            return username == "admin" && password == "password";
        }
    }
}
